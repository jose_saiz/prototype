<?php








/*¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡
 ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ O J O  -  T O D O  -   I M P O R T A N T E !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 TODO Realizar comprobación de que la fecha del sistema es la correcta
 ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡
 ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ O J O  -  T O D O  -   I M P O R T A N T E !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////// D A T E _ T I M E - T O O L S /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function createDateTime(string $date_time):DateTime{
    return DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time);
}

function setDateTime(DateTime &$date_time, string $str_date_time){
    $str = strtotime($str_date_time);
    $year = date('Y',$str);
    $month  = date('m',$str);
    $day = date('d',$str);
    $date_time->setDate($year, $month, $day);
    $hour = date('H',$str);
    $minute = date('i',$str);
    $seconds = date('s',$str);
    $date_time->setTime($hour, $minute,$seconds);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////  S O R T //////////////////////////////////////////////////
function sort_by_hours ($a, $b) {
    return $a['estimated_printing_hours'] - $b['estimated_printing_hours'];
}

function sort_by_hours_desc ($a, $b) {
    return  $b['estimated_printing_hours'] - $a['estimated_printing_hours'];
}

function sort_by_minutes_desc ($a, $b) {
    return  $b['minutes'] - $a['minutes'];
}

function sort_by_datetime ($a, $b) {
    $t1 = strtotime($a['datetime']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['datetime']->format('Y-m-d H:i:s'));
    return $t1 - $t2;
    //return $a['datetime'] - $b['datetime'];
    /*
    if ($a['datetime'] == $b['datetime']) {
        return 0;
    }
    
    return $b['datetime'] > $b['datetime'] ? -1 : 1;
    */
    
}



function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}







function startAscendingComparison($val1, $val2)
{
    if ($val1->start == $val2->start) {
        return 0;
    } else if ($val1->start > $val2->start)
        return 1;
    else
        return - 1;
}


function startDescendingComparison($val1, $val2)
{
    if ($val1->start == $val2->start) {
        return 0;
    } else if ($val1->start < $val2->start)
        return 1;
    else
        return - 1;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////  R A N G E S //////////////////////////////////////////////
function isSizeHours(String $size, String $hours): bool
{
    if (($hours >= conf_parts_range()[$size][0]) && ($hours <= conf_parts_range()[$size][1])) {
        return true;
    } else {
        return false;
    }
}

function isSizeDateTime(String $size, DateTime $start, DateTime $end): bool
{
    $diff = $start->diff($end);

    $hours = $diff->h;
    $hours = $hours + ($diff->days * 24);

    $hours + 1;

    return isSizeHours($size, $hours);

    //return $interval->format('%a Day and %h hours');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////  W E E K E N D ////////////////////////////////////////////
function isWeekend(DateTime $date)
{
    $weekDay = $date->format('w');
    return ($weekDay == 0 || $weekDay == 6);
}

function nextWeekend(string $start_date_time, int $week = 0): array
{
    //Calculo el próximo fin de semana de hoy
    /*
     $saturday= new DateTime('next Saturday');
     $saturday->modify("-30 minutes"); //Viernes 23:30
     if ($week !=0) $saturday->modify("+1 weeks");
     */
    $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$now = new DateTime($start_date_time);
    //$friday = new DateTime('next Friday');
    $friday = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$friday = new DateTime($start_date_time);
    $friday->modify('next Friday');

    if ($now->format('D') == 'Fri') {
        $friday = $now;
    }

    $friday->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
    if ($week != 0){
        $friday->modify("+1 weeks");
    }

    //$sunday = new DateTime('next Sunday');//Como la hora del domingo es 00:00:00 lo cambio 23:59:59

     $sunday = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
     //$sunday = new DateTime($start_date_time);
     /*
     $sunday->modify('next Sunday');
     $sunday->modify("+23 hours");
     $sunday->modify("+59 minutes");
     $sunday->modify("+59 seconds"); //Domingo 23:59
     */
    $sunday->modify('next Monday');
    $sunday->modify("+23 hours");
    $sunday->modify("+59 minutes");
    $sunday->modify("+59 seconds");
    $sunday->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
    return array(
        'friday' => $friday,
        'sunday' => $sunday
    );
}

function nextWeekendFromDate(DateTime $date_time): array
{
    get_object_vars($date_time);
    $friday = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$friday = new DateTime($date_time->date);
    if ($date_time->format('D') != 'Fri') {
        $friday = $friday->modify('next Friday');
    }
    $friday->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);

    $sunday = $date_time->modify('next Sunday'); //Como la hora del domingo es 00:00:00 lo cambio 23:59:59
    $sunday->modify("+23 hours");
    $sunday->modify("+59 minutes");
    $sunday->modify("+59 seconds"); //Domingo 23:59

    return array(
        'friday' => $friday,
        'sunday' => $sunday
    );
}

function nextWeekday(String $day, String $start_date_time): array
{
    //Calculo el próximo fin de semana de hoy

    //$start= new DateTime('next '.$day);
    $start = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$start = new DateTime($start_date_time);
    $start->modify('next ' . $day);
    $start->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
    //$start->modify("-1 second");
    //$end = new DateTime('next '.$day);//Como la hora del domingo es 00:00:00 lo cambio 23:59:59
    $end = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$end = new DateTime($start_date_time);
    $end->modify('next ' . $day);
    $end->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);

    return array(
        'start' => $start,
        'end' => $end
    );
}

////////////////////////////////////////////  E N D  -  W E E K E N D ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////// E S T I M A T E S ///////////////////////////////////////////////////

function estimatedPrintingNightFlex(DateTime $date_time): array
{
    $night = array();
    get_object_vars($date_time);
    //$night['start'] = new DateTime($date_time->date);
    $night['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    if ($date_time->format('H') <= 12) {
        $night['start']->modify('-1 days');
    }
    $day_night_start = $night['start']->format('w');
    $night['start']->setTime(conf_working_days()[$day_night_start]['to']['hour'], conf_working_days()[$day_night_start]['to']['minute'] + conf_delays()['minutes_start']);
    
    get_object_vars($night['start']);
    //$night['end'] = new DateTime($night['start']->date);
    $night['end'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$night['start']->format('Y-m-d H:i:s'));
    $night['end']->modify('+1 days');
    $day_night_end = $night['end']->format('w');
    $night['end']->setTime(conf_working_days()[$day_night_end]['from']['hour'], conf_working_days()[$day_night_end]['from']['minute'] + conf_delays()['minutes_start']);
    
    return $night;
}

function estimatedPrintingNightsFlex(array $estimated_printing): array
{

    //Night Before
    get_object_vars($estimated_printing['start']);
    $night_before_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
    //$night_before_start = new DateTime($estimated_printing['start']->date);
    
    //Si la hora estimada es menor de 12
    /*
    if ($estimated_printing['start']->format('H') <= 12) {
        $night_before_start->modify('-1 days');
    }
    */
    
    
    $day_start = $night_before_start->format('w');
    $night_before_start->setTime(conf_working_days()[$day_start]['to']['hour'], conf_working_days()[$day_start]['to']['minute'] /*+ conf_delays()['minutes_start']*/);

    get_object_vars($estimated_printing['end']);
    $night_before_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
    //$night_before_end = new DateTime($estimated_printing['start']->date);
    if ($estimated_printing['start']->format('H') > 12) {
        $night_before_end->modify('+1 days');
    }
    $day_end = $night_before_start->format('w');
    $night_before_end->setTime(conf_working_days()[$day_end]['from']['hour'], conf_working_days()[$day_end]['from']['minute'] /*+ conf_delays()['minutes_start']*/);
    get_object_vars($night_before_end);

    //Night Behind
    $night_behind_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['end']->format('Y-m-d H:i:s'));
    //$night_behind_start = new DateTime($estimated_printing['end']->date);

    if ($estimated_printing['end']->format('H') <= 12) {
        $night_behind_start->modify('-1 days');
    } else {}
    $day_behind_start = $night_behind_start->format('w');
    $night_behind_start->setTime(conf_working_days()[$day_behind_start]['to']['hour'], conf_working_days()[$day_behind_start]['to']['minute'] /*+ conf_delays()['minutes_start']*/);

    get_object_vars($night_behind_start);

    $night_behind_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$night_behind_start->format('Y-m-d H:i:s'));
    //$night_behind_end = new DateTime($night_behind_start->date);
    if ($estimated_printing['end']->format('H') > 12) {} else {}

    $night_behind_end->modify('+1 days');
    $day_behind_end = $night_behind_end->format('w');
    $night_behind_end->setTime(conf_working_days()[$day_behind_end]['from']['hour'], conf_working_days()[$day_behind_end]['from']['minute'] /*+ conf_delays()['minutes_start']*/);

    $night = array(
        'before' => array(
            'start' => $night_before_start,
            'end' => $night_before_end
        ),
        'behind' => array(
            'start' => $night_behind_start,
            'end' => $night_behind_end
        )
    );
    return $night;
}





function estimatedPrintingNights(array $estimated_printing): array
{

    //Night Before
    get_object_vars($estimated_printing['start']);
    $night_before_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
    //$night_before_start = new DateTime($estimated_printing['start']->date);
    $night_before_start->setTime(conf_workday()['to_hour'], conf_workday()['to_minute'] + conf_delays()['minutes_start']);
    //Si la hora estimada es menor de 12

    if ($estimated_printing['start']->format('H') <= 12) {
        $night_before_start->modify('-1 days');
    }

    get_object_vars($estimated_printing['end']);
    $night_before_end =  DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
    //$night_before_end = new DateTime($estimated_printing['start']->date);
    if ($estimated_printing['start']->format('H') > 12) {
        $night_before_end->modify('+1 days');
    }
    $night_before_end->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
    get_object_vars($night_before_end);

    //Night Behind
    $night_behind_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['end']->format('Y-m-d H:i:s'));
    //night_behind_start = new DateTime($estimated_printing['end']->date);

    if ($estimated_printing['end']->format('H') <= 12) {
        $night_behind_start->modify('-1 days');
    } else {}
    $night_behind_start->setTime(conf_workday()['to_hour'], conf_workday()['to_minute'] + conf_delays()['minutes_start']);

    get_object_vars($night_behind_start);

    $night_behind_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$night_behind_start->format('Y-m-d H:i:s'));
    //$night_behind_end = new DateTime($night_behind_start->date);
    if ($estimated_printing['end']->format('H') > 12) {} else {}

    $night_behind_end->modify('+1 days');

    $night_behind_end->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] /*+conf_delays()['minutes_start']*/
    );

    $night = array(
        'before' => array(
            'start' => $night_before_start,
            'end' => $night_before_end
        ),
        'behind' => array(
            'start' => $night_behind_start,
            'end' => $night_behind_end
        )
    );
    return $night;
}

function estimatedPrintingNightsTmp(array $estimated_printing): array
{
    get_object_vars($estimated_printing['start']);
    $night_before_start = new DateTime($estimated_printing['start']->date);
    $night_before_start->setTime(conf_workday()['to_hour'], conf_workday()['to_minute'] + conf_delays()['minutes_start']);
    //$night_before_start->modify ('-1 days');
    get_object_vars($estimated_printing['end']);
    $night_before_end = new DateTime($estimated_printing['start']->date);
    $night_before_end->modify('+1 days');
    $night_before_end->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
    get_object_vars($night_before_end);

    $night_behind_start = new DateTime($estimated_printing['end']->date);
    //$night_behind_start ->modify('-1 days');
    $night_behind_start->setTime(conf_workday()['to_hour'], conf_workday()['to_minute'] + conf_delays()['minutes_start']);
    get_object_vars($night_behind_start);
    $night_behind_end = new DateTime($night_behind_start->date);
    $night_behind_end->modify('+1 days');
    $night_behind_end->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
    $night = array(
        'before' => array(
            'start' => $night_before_start,
            'end' => $night_before_end
        ),
        'behind' => array(
            'start' => $night_behind_start,
            'end' => $night_behind_end
        )
    );
    return $night;
}

/*
 function estimatedPrintingNights(array $estimated_printing): array{
 get_object_vars($estimated_printing['start']);
 $night_before_start = new DateTime($estimated_printing['start']->date);
 $night_before_start->setTime(conf_workday()['to_hour'],conf_workday()['to_minute']+conf_delays()['minutes_start']);
 //$night_before_start->modify ('-1 days');
 $night_before_end = new DateTime($estimated_printing['start']->date);
 $night_before_end ->modify ('+1 days');
 $night_before_end->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']);
 get_object_vars($night_before_end);
 $night_behind_start = new DateTime($night_before_end->date);
 //$night_behind_start ->modify('+1 days');
 $night_behind_start->setTime(conf_workday()['to_hour'],conf_workday()['to_minute']+conf_delays()['minutes_start']);
 get_object_vars($night_behind_start);
 $night_behind_end = new DateTime($night_behind_start->date);
 $night_behind_end ->modify ('+1 days');
 $night_behind_end->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']);
 $night = array ('before' => array('start'=>$night_before_start,'end'=>$night_before_end),
 'behind' => array('start'=>$night_behind_start,'end'=>$night_behind_end)
 );
 return $night;
 }
 */
function estimatedPrintingEnd(DateTime $estimated_printing_start, array $pending_part): DateTime
{
    $estimated_printing_end = new DateTime($estimated_printing_start->format('Y-m-d H:i:s'));
    $estimated_printing_end->modify("+" . $pending_part['estimated_printing_hours'] . " hours");
    $estimated_printing_end->modify("+" . $pending_part['estimated_printing_minutes'] . " minutes");
    return $estimated_printing_end;
}
function estimatedPrintingEndSlot(DateTime $estimated_printing_start,PrinterSlot $slot): DateTime
{
    return estimatedPrintingEnd($estimated_printing_start, $slot->toPendingPart());
}

function estimatedPrintingStart(DateTime $estimated_printing_end, array $pending_part): DateTime
{
    $estimated_printing_start = new DateTime($estimated_printing_end->format('Y-m-d H:i:s'));
    $estimated_printing_start->modify("-" . $pending_part['estimated_printing_hours'] . " hours");
    $estimated_printing_start->modify("-" . $pending_part['estimated_printing_minutes'] . " minutes");
    return $estimated_printing_start;
}

function estimatedPrintingStartSlot(DateTime $estimated_printing_end,PrinterSlot $slot): DateTime
{
    return estimatedPrintingStart($estimated_printing_end, $slot->toPendingPart());
}


function workDay(DateTime $estimated_printing): array
{
    $workday_start = new DateTime($estimated_printing->format('Y-m-d'));
    $workday_start->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
    $workday_end = new DateTime($estimated_printing->format('Y-m-d'));
    $workday_end->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);

    return array(
        'start' => $workday_start,
        'end' => $workday_end
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////  R A N G O S ///////////////////////////////////////////////
function calcMinutes($minutes)
{
    $time = ['days' => 0, 'hours' => 0, 'minutes' => 0];
    
    while ($minutes >= 60) {
        
        if ($minutes >= 1440) {
            
            $time['days']++;
            $minutes = $minutes - 1440;
        } else if ($minutes >= 60) {
            
            $time['hours']++;
            $minutes = $minutes - 60;
        }
    }
    
    $time['minutes'] = $minutes;
    
    return $time;
}



function rangeTotalM(DateTime $start, DateTime $end)
{
    $interval = $start->diff($end);
    $days = $interval->format('%d');
    $hours = $interval->format('%h');
    $minutes = $interval->format('%i');
    return $days * 24 * 60 + $hours * 60 + $minutes;
}


function diffHours(DateTime $date1, DateTime $date2)
{
    $diff = $date2->diff($date1);
    
    $hours = $diff->h;
    $hours = $hours + ($diff->days * 24);
    return $hours;
}

function diffMinutes(DateTime $start, DateTime $end):int
{
    $hours = $start->diff($end)->format('%h');
    $minutes = $start->diff($end)->format('%i');
    
    if ($start->diff($end)->format('%d') >=1){
        $hours += $start->diff($end)->format('%d')*24;
    }
    
    
    return $hours*60 + $minutes;
}

function diffRangeHM(DateTime $start, DateTime $end):array
{
    $hours = $start->diff($end)->format('%h');
    $minutes = $start->diff($end)->format('%i');
    
    if ($start->diff($end)->format('%d') >=1){
        $hours += $start->diff($end)->format('%d')*24;
    }
        

    return array(
        'hours' => $hours,
        'minutes' => $minutes
    );
}

function diffDays(DateTime $date1, DateTime $date2)
{
    $diff = $date2->diff($date1);
    return $diff->days;;
}

function subtractRangeHM(DateTime $date_time, array $range): DateTime
{
    $substract_date_time = new DateTime($date_time->format('Y-m-d H:i:s'));
    $substract_date_time->modify("-" . $range['hours'] . " hours");
    $substract_date_time->modify("-" . $range['minutes'] . " minutes");

    return $substract_date_time;
}

function addRangeHM(DateTime $date_time, array $range): DateTime
{
    $add_date_time = new DateTime($date_time->format('Y-m-d H:i:s'));
    $add_date_time->modify("+" . $range['hours'] . " hours");
    $add_date_time->modify("+" . $range['minutes'] . " minutes");

    return $add_date_time;
}

function estimatedPrintedInNextWeekend(array $pending_part, string $start_date_time, int $week = 0): array
{
    //Calculo el próximo fin de semana de hoy para calcular la fecha estimada
    $next_weekend = nextWeekend($start_date_time, $week);
    //Calculo la fecha estimada de incio y fin de la pieza actual, en función de la fecha del próximo sábado
    $estimated_printing_start = $next_weekend['friday'];

    $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);

    //Comprueba que la tarea no acaba fuera del horario de trabajo.
    $workday = workDay($estimated_printing_end);
    //Miro que la fecha fin (estimada) no se solape con la noche. Si es así ajusto la fecha estimada para que no acabe por la noche.
    if ($estimated_printing_end < $workday['start']) {

        //$estimated_printing_end_2 = new DateTime($estimated_printing_end->format('Y-m-d H:i:s'));
        //Calculo la nueva fecha fin: Resto la jornada anterior a la fecha fin que no nos ha servido, solo se diferencian en horas y minutos
        $workday['end']->modify("-1 day");
        //Calculo la diferencia de horas entre la fecha que había estimado y el fin de la jornada. Es lo que tengo que desplazar la estimada.
        $range = diffRangeHM($estimated_printing_end, $workday['end']);
        $estimated_printing_end_2 = subtractRangeHM($estimated_printing_end, $range);
        $estimated_printing_start_2 = estimatedPrintingStart($estimated_printing_end_2, $pending_part);

        return array(
            'start' => $estimated_printing_start_2,
            'end' => $estimated_printing_end_2
        );
    }
    //TODO Comprobar que la nueva fecha incio no sea por la noche en cuyo caso habrá que ir probando
    //Comprueba que la fecha inicio estimada no se solapa con la noche
    return array(
        'start' => $estimated_printing_start,
        'end' => $estimated_printing_end
    );
}

function estimatedPrintedInNextWeekendSlot(array $pending_part, PrinterSlot $printerSlot): array
{
    //Calculo el próximo fin de semana despues del slot.
    $printer_slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
    //$printer_slot_end = new DateTime($printerSlot->end);
    $next_weekend = nextWeekendFromDate($printer_slot_end);

    //Calculo la fecha estimada de incio y fin de la pieza actual, en función de la fecha del próximo sábado
    $estimated_printing_start = $next_weekend['friday'];

    $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);

    //Comprueba que la tarea no acaba fuera del horario de trabajo.
    $workday = workDay($estimated_printing_end);
    //Miro que la fecha fin (estimada) no se solape con la noche. Si es así ajusto la fecha estimada para que no acabe por la noche.
    if ($estimated_printing_end < $workday['start']) {

        //$estimated_printing_end_2 = new DateTime($estimated_printing_end->format('Y-m-d H:i:s'));
        //Calculo la nueva fecha fin: Resto la jornada anterior a la fecha fin que no nos ha servido, solo se diferencian en horas y minutos
        $workday['end']->modify("-1 day");
        //Calculo la diferencia de horas entre la fecha que había estimado y el fin de la jornada. Es lo que tengo que desplazar la estimada.
        $range = diffRangeHM($estimated_printing_end, $workday['end']);
        $estimated_printing_end_2 = subtractRangeHM($estimated_printing_end, $range);
        $estimated_printing_start_2 = estimatedPrintingStart($estimated_printing_end_2, $pending_part);

        return array(
            'start' => $estimated_printing_start_2,
            'end' => $estimated_printing_end_2
        );
    }
    //TODO Comprobar que la nueva fecha incio no sea por la noche en cuyo caso habrá que ir probando
    //Comprueba que la fecha inicio estimada no se solapa con la noche
    return array(
        'start' => $estimated_printing_start,
        'end' => $estimated_printing_end
    );
}

function estimatedPrintedBig(array $pending_part, string $start_date_time, $day, $week = 0): array
{
    //Calculo el rango hacia atrás: Primero busco el final al principio de la jornada del Viernes y resto a esa fecha el tiempo estimado de la pieza
    //Caculo cuando empiza el día laboral siguiente, que será cuando tiene que acabar la impresión

    //$estimated_printing_end= new DateTime('next '.$day);
    $estimated_printing_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$estimated_printing_end = new DateTime($start_date_time);
    $estimated_printing_end->modify('next ' . $day);

    if ($week != 0) {
        $estimated_printing_end->modify("+" . $week . " weeks");
    }

    $estimated_printing_end = addRangeHM($estimated_printing_end, array(
        'hours' => conf_workday()['from_hour'],
        'minutes' => conf_workday()['from_minute']
    ));
    $estimated_printing_start = estimatedPrintingStart($estimated_printing_end, $pending_part);

    //Comprueba que la tarea no acaba fuera del horario de trabajo(por la noche).
    //Si la fecha asignada es del pasado.
    $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$now = new DateTime($start_date_time);
    if ($estimated_printing_start < $now) {

        $estimated_printing_start_2 = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
        //$estimated_printing_start_2 = new DateTime($start_date_time);
        $estimated_printing_start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
        /*
         if (is_overlap_at_night($estimated_printing_start_2)) {
         if ($estimated_printing_start_2->format('H') > conf_workday()['from_hour'] ){
         $estimated_printing_start_2 -> modify('+1 days');
         }
         }
         */
        $estimated_printing_start = $estimated_printing_start_2;
        $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);
    }

    $workday = workDay($estimated_printing_end);

    //El tiempo estimado inicial, es fin de semana
    if ((isWeekend($estimated_printing_start) || isWeekend($estimated_printing_end))) {

        //Calculo la nueva fecha fin: Resto la jornada anterior a la fecha fin que no nos ha servido, solo se diferencian en horas y minutos
        //Lo muevo al final de  viernes de la siguiente semana

        if ($week != 0) {
            //Si no es la primera semana
            $estimated_printing_start_2 = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing_end->format('Y-m-d H:i:s'));
            //$estimated_printing_start_2 = new DateTime($estimated_printing_end->format('Y-m-d H:i:s'));
            $estimated_printing_start_2->modify('+' . $week . ' weeks');
            //Calculo la nueva fecha fin: Resto la jornada anterior a la fecha fin que no nos ha servido, solo se diferencian en horas y minutos
            $workday['end']->modify('+' . $week . ' weeks');
            $workday['end']->modify("-1 day");

            $range = diffRangeHM($estimated_printing_end, $workday['end']);
            $estimated_printing_end_2 = subtractRangeHM($estimated_printing_end, $range);
            $estimated_printing_start_2 = estimatedPrintingStart($estimated_printing_end_2, $pending_part);

            //$week ++;
        } else {
            //Es laprimera semana
            //$estimated_printing_end_2 = new DateTime("next Friday");
            $estimated_printing_end_2 = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
            //$estimated_printing_end_2 = new DateTime($start_date_time);
            $estimated_printing_end->modify('next Friday');

            $estimated_printing_end_2->modify("+ 1 weeks");

            $estimated_printing_end_2 = addRangeHM($estimated_printing_end_2, array(
                'hours' => conf_workday()['from_hour'],
                'minutes' => conf_workday()['from_minute']
            ));
            $estimated_printing_start_2 = estimatedPrintingStart($estimated_printing_end_2, $pending_part);
        }

        $estimated_printing_start = $estimated_printing_start_2;
        $estimated_printing_end = $estimated_printing_end_2;
    }

    //Miro que la fecha final(estimada) no sea por la noche
    if (($estimated_printing_end < $workday['start'])) {
        $estimated_printing_end_2 = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing_end->format('Y-m-d H:i:s'));
        //$estimated_printing_end_2 = new DateTime($estimated_printing_end->format('Y-m-d H:i:s'));
        //Calculo la nueva fecha fin: Resto la jornada anterior a la fecha fin que no nos ha servido, solo se diferencian en horas y minutos
        //$workday['start']->modify("-1 day");

        $range = diffRangeHM($estimated_printing_end, $workday['end']);
        $estimated_printing_end_2 = subtractRangeHM($estimated_printing_end, $range);
        $estimated_printing_start_2 = estimatedPrintingStart($estimated_printing_end_2, $pending_part);

        $estimated_printing_start = $estimated_printing_start_2;
        $estimated_printing_end = $estimated_printing_end_2;
    }
    //TODO Comprobar que la nueva fecha inicio no sea por la noche en cuyo caso habrá que ir probando
    /*
     if (is_overlap_at_night($estimated_printing_start)) {
     $estimated_printing_start_2 = new DateTime($estimated_printing_start->format('Y-m-d H:i:s'));
     if ($estimated_printing_start_2->format('H') > conf_workday()['from_hour'] ){
     $estimated_printing_start_2 -> modify('+1 days');
     }
     $estimated_printing_start_2->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']);
     $estimated_printing_start = $estimated_printing_start_2;
     $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);
     }
     */
    return array(
        'start' => $estimated_printing_start,
        'end' => $estimated_printing_end
    );
}

function estimatedPrintedMedianFromStart(array $pending_part, DateTime $estimated_printing_start, string $start_date_time): array
{
    $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);

    //Si la fecha asignada es del pasado.

    removeOverlapPast($pending_part, $estimated_printing_start, $estimated_printing_end, $start_date_time);

    //Miro si el inicio de la fecha estimada es fin de semana

    if (isWeekend($estimated_printing_start)) {

        get_object_vars($estimated_printing_start);
        $estimated_printing_start_2 = new DateTime($estimated_printing_start->date);
        $estimated_printing_start_2->modify("next Monday");
        $estimated_printing_start_2 = addRangeHM($estimated_printing_start_2, array(
            'hours' => conf_workday()['from_hour'],
            'minutes' => conf_workday()['from_minute'] + 1
        ));

        $estimated_printing_end_2 = estimatedPrintingEnd($estimated_printing_start_2, $pending_part);

        $estimated_printing_start = $estimated_printing_start_2;
        $estimated_printing_end = $estimated_printing_end_2;
    }
    //Miro si el fin de la fecha estimada es fin de semana

    if (isWeekend($estimated_printing_end)) {
        $estimated_printing_start_2 = addRangeHM($estimated_printing_end->modify("next Monday"), array(
            'hours' => conf_workday()['from_hour'],
            'minutes' => conf_workday()['from_minute'] + 1
        ));

        $estimated_printing_end_2 = estimatedPrintingEnd($estimated_printing_start_2, $pending_part);

        $estimated_printing_start = $estimated_printing_start_2;
        $estimated_printing_end = $estimated_printing_end_2;
    }

    //TODO Mirar que la fecha inicio(estimada) no sea por la noche
    /*
     $estimated_printing_aux = array('start'=>$estimated_printing_start,'end'=>$estimated_printing_end);
     if (removeOverlapAtNight($estimated_printing_aux, $pending_part)){
     return $estimated_printing_aux;
     }else{
     $message =  "Warning: There is a task that ends or starts at night and it is not possible to correct it:\n".
     'Id:'.$pending_part['id'].'\n'.
     'File:'.$pending_part['id_file'].'\n';
     echo "<script type='text/javascript'> alert('$message');</script>";

     return array('start'=>$estimated_printing_start,'end'=> $estimated_printing_end) ;
     }

     */

    $estimated_printing = array(
        'start' => $estimated_printing_start,
        'end' => $estimated_printing_end
    );

    get_object_vars($estimated_printing['end']);
    get_object_vars($estimated_printing['start']);
    //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
    if (! removeOverlapAtNightMedian($estimated_printing, $pending_part, $start_date_time)) {}
    //O J O: LLAMADA RECURSIVA - Se cuelga 
    removeOverlapPastFromStart($pending_part, $estimated_printing, $start_date_time);

    return $estimated_printing;
}

function estimatedPrintedFromStart(array $pending_part, DateTime $estimated_printing_start, string $start_date_time): array
{
    
    $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);
    
    //Si la fecha asignada es del pasado.
    
    //removeOverlapPast($pending_part, $estimated_printing_start, $estimated_printing_end, $start_date_time);
    
    //Miro si el inicio de la fecha estimada es fin de semana
    

    $estimated_printing = array(
        'start' => $estimated_printing_start,
        'end' => $estimated_printing_end
    );
    
    get_object_vars($estimated_printing['end']);
    get_object_vars($estimated_printing['start']);
    //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
    if (! removeOverlapAtNight($estimated_printing, $pending_part, $start_date_time)) {}
    //removeOverlapPastFromStart($pending_part, $estimated_printing, $start_date_time);
    
    return $estimated_printing;
}


////////////////////////////////////////////  E N D  -  E S T I M A T E S ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// O V E R L A P //////////////////////////////////////////////
function datesOverlap($start_one, $end_one, $start_two, $end_two)
{
    if ($start_one <= $end_two && $end_one >= $start_two) { //If the dates overlap
        return min($end_one, $end_two)->diff(max($start_two, $start_one))->days + 1; //return how many days overlap
    }

    return 0; //Return 0 if there is no overlap
}

function is_overlap(DateTime $start_time1, DateTime $end_time1, DateTime $start_time2, DateTime $end_time2)
{
    $result = (($start_time1) <= ($end_time2) && ($start_time2) <= ($end_time1) ? true : false);
    return $result;
}

function is_overlap_whithout_wrap(DateTime $start_time1, DateTime $end_time1, DateTime $start_time2, DateTime $end_time2)
{
    //Si la datetime  2 envuelve al 1
    if (($start_time2 < $start_time1) && ($end_time2 > $end_time1)) {
        return false;
    } 
    /* //Si la datetime  1 envuelve al 2
    if (($start_time2 > $start_time1) && ($end_time2 < $end_time1)) {
        return false;
    }
    */
    return is_overlap($start_time1, $end_time1, $start_time2, $end_time2);
}


function is_overlap_or_wrap(DateTime $start_time1, DateTime $end_time1, DateTime $start_time2, DateTime $end_time2)
{
    //Si la datetime  2 envuelve al 1
    if (($start_time2 < $start_time1) && ($end_time2 > $end_time1)) {
        return true;
    }
    //Si la datetime  1 envuelve al 2
    if (($start_time2 > $start_time1) && ($end_time2 < $end_time1)) {
        return true;
    }
    return is_overlap($start_time1, $end_time1, $start_time2, $end_time2);
}

function is_wrap(DateTime $start_time1, DateTime $end_time1, DateTime $start_time2, DateTime $end_time2)
{
    if (($start_time2 < $start_time1) && ($end_time2 > $end_time1)) {
        return false;
    } else {
        return true;
    }
}

function is_overlapRange($time1, $time2)
{
    $result = is_overlap($time1['start'], $time1['start'], $time2['start'], $time2['start']);
    return $result;
}

function isNearStartNight(DateTime $date_time)
{
    get_object_vars($date_time);
    
    $night_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night = new DateTime($date_time->date);
    $day = $night_start->format('w');
    $night_start->setTime(conf_working_days()[$day]['to']['hour'], conf_working_days()[$day]['to']['minute']);
    if ($date_time->format('H') <= 12) {
        $night_start->modify('-1 days');
    }
    $night_end = createDateTime($night_start->format('Y-m-d H:i:s'));
    $night_end->modify('+3 hours');
    return ((($date_time) > ($night_start)) &&  (($date_time) < ($night_end)));
    
}

function isNearEndNight(DateTime $date_time)
{
    get_object_vars($date_time);
    
    $night_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_from = new DateTime($date_time->date);
    $day = $night_end->format('w');
    $night_end->setTime(conf_working_days()[$day]['from']['hour'], conf_working_days()[$day]['from']['minute']);
    if ($date_time->format('H') > 12) {
        $night_end->modify('+1 days');
    }
    $night_start = createDateTime($night_end->format('Y-m-d H:i:s'));
    $night_start->modify('-3 hours');
    return ((($date_time) > ($night_start)) &&  (($date_time) < ($night_end)));
    
}

function is_overlap_at_night(DateTime $date_time):bool
{
    get_object_vars($date_time);

    $night_from = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_from = new DateTime($date_time->date);
    $night_from->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
    if ($date_time->format('H') <= 12) {
        $night_from->modify('-1 days');
    }
    $night_to = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_to = new DateTime($date_time->date);

    if ($date_time->format('H') > 12) {
        $night_to->modify('+1 days');
    }
    $night_to->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);

    $result = (($date_time) <= ($night_to) && ($night_from) <= ($date_time) ? true : false);
    return $result;
}

function isOverlapAtNight(DateTime $date_time)
{
    get_object_vars($date_time);
    
    $night_from = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_from = new DateTime($date_time->date);
    $day_from = $night_from->format('w');
    $night_from->setTime(conf_working_days()[$day_from]['to']['hour'], conf_working_days()[$day_from]['to']['minute']);
    if ($date_time->format('H') <= 12) {
        $night_from->modify('-1 days');
    }

    $night_to = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_to = new DateTime($date_time->date);
    $day_to = $night_to->format('w');
    $night_to->setTime(conf_working_days()[$day_to]['from']['hour'], conf_working_days()[$day_to]['from']['minute']);
    if ($date_time->format('H') > 12) {
        $night_to->modify('+1 days');
    }

    
    $result = (($date_time) <= ($night_to) && ($night_from) <= ($date_time) ? true : false);
    return $result;
}

function isOverlapAtNightFlex(DateTime $date_time)
{
    get_object_vars($date_time);
    
    $night_from = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_from = new DateTime($date_time->date);
    $day_from = $night_from->format('w');
    $night_from->setTime(conf_working_days()[$day_from]['to']['hour'], conf_working_days()[$day_from]['to']['minute']);
    if ($date_time->format('H') <= 12) {
        $night_from->modify('-1 days');
    }
    
    $night_to = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_to = new DateTime($date_time->date);
    $day_to = $night_to->format('w');
    $night_to->setTime(conf_working_days()[$day_to]['from']['hour'], conf_working_days()[$day_to]['from']['minute']);
    if ($date_time->format('H') > 12) {
        $night_to->modify('+1 days');
    }
    
    //$result = (($date_time) <= ($night_to) && ($night_from) <= ($date_time) ? true : false);
    $result = (($date_time) < ($night_to) && ($night_from) < ($date_time) ? true : false);
    return $result;
}


function OverlapAtNight(DateTime $date_time):array
{
    get_object_vars($date_time);
    
    $night_from = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_from = new DateTime($date_time->date);
    $day_from = $night_from->format('w');
    $night_from->setTime(conf_working_days()[$day_from]['to']['hour'], conf_working_days()[$day_from]['to']['minute']);
    if ($date_time->format('H') <= 12) {
        $night_from->modify('-1 days');
    }
    
    $night_to = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
    //$night_to = new DateTime($date_time->date);
    $day_to = $night_to->format('w');
    $night_to->setTime(conf_working_days()[$day_to]['from']['hour'], conf_working_days()[$day_to]['from']['minute']);
    if ($date_time->format('H') > 12) {
        $night_to->modify('+1 days');
    }
    
    $result = (($date_time) <= ($night_to) && ($night_from) <= ($date_time) ? true : false);
    
    if ($result){
        return array('night_from'=>$night_from,'night_to'=>$night_to);
    }
    return  false;
}

function removeOverlapPast(array $pending_part, DateTime &$estimated_printing_start, DateTime &$estimated_printing_end, string $start_date_time)
{
    $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$now = new DateTime($start_date_time);
    if ($estimated_printing_start < $now) {
        $estimated_printing_start->modify("+" . conf_delays()['minutes_start'] . " minutes");
        $estimated_printing_end = estimatedPrintingEnd($estimated_printing_start, $pending_part);
    }
}

function removeOverlapPastFromStart(array $pending_part, array &$estimated_printing, string $start_date_time)
{
    $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$now = new DateTime($start_date_time);
    if ($estimated_printing['start'] < $now) {
        $now->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
        $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $now, $start_date_time);
    }
}


function removeOverlapPastFromStartAll(array $pending_part, array &$estimated_printing, string $start_date_time)
{
    $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$now = new DateTime($start_date_time);
    if ($estimated_printing['start'] < $now) {
        //$day_now = $now->format('w');
        //$now->setTime(conf_working_days()[$day_now]['from']['hour'], conf_working_days()[$day_now]['from']['minute']);
        
        //$now->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
        $estimated_printing['start'] = $now;
        $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        
        if (isOverlapAtNight($estimated_printing['end'])){
            //$start_night = estimatedPrintingNightFlex($estimated_printing['start']);
            
             
            
            $day_start = $estimated_printing['start']->format('w');
            
            $estimated_printing['start']->setTime(conf_working_days()[$day_start]['to']['hour'], conf_working_days()[$day_start]['to']['minute']- conf_delays()['minutes_start']);
            $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
            /*
            $behind_night   = estimatedPrintingNightFlex($estimated_printing['end']);
            
            $TotalMinutesNight = rangeTotalM($behind_night['start'], $behind_night['end']);
            $TotalMinutesPart = rangeTotalM($estimated_printing['start'], $estimated_printing['end']);
            
            $right_offset = intdiv($TotalMinutesPart - $TotalMinutesNight,2);
            
            $estimated_printing['end'] = $behind_night['end'];
            $estimated_printing['end']->modify('+'. $right_offset.' minutes');
            
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
            */
        }
        
    }else{
        
    }
}

function removeOverlapAtNightBig(array &$estimated_printing, array $pending_part, string $start_date_time): bool
{
    //Comprobar que la nueva estimación no se solapa con la noche de incio o la de fin,
    //Si se solapa dependiendo de si es por delante o por detrás recalculamos la fecha estimada.
    $night = estimatedPrintingNights($estimated_printing);

    if (is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) {
        ///Si se solapa por delante adelanto la hora estimada
        if (intval($estimated_printing['start']->format('H')) <= intval(conf_workday()['from_hour'])) {
            //Si la hora que se había estimado es mayor que la del inicio de jornada, podemos ajustar a por el inicio
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
        } else {
            //Si la hora que se había estimado no es mayor que la del inicio de jornada, podemos ajustar al día siguiente del inicio de la fecha estimada
            $estimated_printing['start']->modify('+1 days');
            $estimated_printing['start']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);

            $pending_part_aux = $pending_part;
            $pending_part_aux['estimated_printing_minutes'] ++;
            $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part_aux);
        }
    } elseif (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end'])) {
        ///Si se solapa por  detras también adelanto la hora estimada, pero tomando como referencia el final
        //$message =  "Warning: No se había detectado esta situación (Apuntar datos y analizarlos)";
        //echo "<script type='text/javascript'> alert('$message');</script>";
        if (intval($estimated_printing['start']->format('H')) <= intval(conf_workday()['from_hour'])) {

            $estimated_printing['end']->modify('+1 days');
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);

            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
            /*
             $estimated_printing['start'] ->modify('+1 days');
             $estimated_printing['start'] ->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']+1);

             $pending_part_aux = $pending_part;
             $pending_part_aux['estimated_printing_minutes']++;
             $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part_aux);
             */
        } else {
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
        }
    } else {
        //Si no se ha solapado
        return false;
    }
    $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time);
    //$now = new DateTime($start_date_time);
    //Comprobar que la nueva fecha de incio no es del pasado.
    if ($now > $estimated_printing['start']) {
        $estimated_printing['end']->modify('+1 days');

        $estimated_printing['end']->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
        $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);

        //La nueva fecha puede solaparse con la noche anterior de la fecha de inicio.
        //Calculo la noche del día anterior a la fecha de inicio estimada
        get_object_vars($estimated_printing['start']);
        $estimated_printing_aux = array();
        $estimated_printing_aux['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
        //$estimated_printing_aux['start'] = new DateTime($estimated_printing['start']->date);
        $estimated_printing_aux['start']->modify('-1 days');
        $estimated_printing_aux['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        $night_aux = estimatedPrintingNights($estimated_printing_aux);

        if (is_overlap_whithout_wrap($night_aux['before']['start'], $night_aux['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) {
            //$estimated_printing['start'] = $estimated_printing['end'];
            $estimated_printing['start']->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
            $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        }
    }

    return true;
}

function removeOverlapAtNightMedianNEW(array &$estimated_printing, array $pending_part, string $start_date_time): bool
{
    //Comprobar que la nueva estimación no se solapa con la noche de incio o la de fin,
    //Si se solapa dependiendo de si es por delante o por detrás recalculamos la fecha estimada.
    $night = estimatedPrintingNights($estimated_printing);

    if (is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) {
        ///Si se solapa por delante adelanto la hora estimada
        if (intval($estimated_printing['start']->format('H')) <= intval(conf_workday()['from_hour'])) {
            //Si la hora que se había estimado es mayor que la del inicio de jornada, podemos ajustar a por el inicio
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
        } else {
            //Si la hora que se había estimado no es mayor que la del inicio de jornada, podemos ajustar al día siguiente del inicio de la fecha estimada
            $estimated_printing['start']->modify('+1 days');
            $estimated_printing['start']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);

            $pending_part_aux = $pending_part;
            $pending_part_aux['estimated_printing_minutes'] ++;
            $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part_aux);
        }
    } elseif (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end'])) {
        ///Si se solapa por  detras también adelanto la hora estimada, pero tomando como referencia el final
        //$message =  "Warning: No se había detectado esta situación (Apuntar datos y analizarlos)";
        //echo "<script type='text/javascript'> alert('$message');</script>";
        if (intval($estimated_printing['start']->format('H')) <= intval(conf_workday()['from_hour'])) {

            //$estimated_printing['end'] ->modify('+1 days');
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);

            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
            /*
             $estimated_printing['start'] ->modify('+1 days');
             $estimated_printing['start'] ->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']+1);

             $pending_part_aux = $pending_part;
             $pending_part_aux['estimated_printing_minutes']++;
             $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part_aux);
             */
        } else {
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
        }
    } else {
        //Si no se ha solapado
        return false;
    }
    $now = new DateTime($start_date_time);
    //Comprobar que la nueva fecha de incio no es del pasado.
    if ($now > $estimated_printing['start']) {
        $estimated_printing['end']->modify('+1 days');

        $estimated_printing['end']->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
        $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);

        //La nueva fecha puede solaparse con la noche anterior de la fecha de inicio.
        //Calculo la noche del día anterior a la fecha de inicio estimada
        get_object_vars($estimated_printing['start']);
        $estimated_printing_aux = array();
        $estimated_printing_aux['start'] = new DateTime($estimated_printing['start']->date);
        $estimated_printing_aux['start']->modify('-1 days');
        $estimated_printing_aux['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        $night_aux = estimatedPrintingNights($estimated_printing_aux);

        if (is_overlap_whithout_wrap($night_aux['before']['start'], $night_aux['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) {
            //$estimated_printing['start'] = $estimated_printing['end'];
            $estimated_printing['start']->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
            $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        }
    }

    return true;
}

function removeOverlapAtNightMedian(array &$estimated_printing, array $pending_part, string $start_date_time): bool
{
    //Comprobar que la nueva estimación no se solapa con la noche de incio o la de fin,
    //Si se solapa dependiendo de si es por delante o por detrás recalculamos la fecha estimada.
    $night = estimatedPrintingNights($estimated_printing);

    if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) && (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
        //echo "point";
    };

    if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) || (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
        $estimated_printing_aux = array();
        get_object_vars($estimated_printing['end']);
        
        $estimated_printing_aux['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
        //$estimated_printing_aux['start'] = new DateTime($estimated_printing['start']->date);
        $estimated_printing_aux['start']->modify('+1 days');
        $estimated_printing_aux['start']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);

        $pending_part_aux = $pending_part;
        $pending_part_aux['estimated_printing_minutes'] ++;
        $estimated_printing_aux['end'] = estimatedPrintingEnd($estimated_printing_aux['start'], $pending_part_aux);

        $night = estimatedPrintingNights($estimated_printing_aux);

        if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing_aux['start'], $estimated_printing_aux['end'])) || (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing_aux['start'], $estimated_printing_aux['end']))) {
            $estimated_printing['end']->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] + 1);
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
        } else {
            $estimated_printing = $estimated_printing_aux;
        }
    } else {
        //Si no se ha solapado
        return false;
    }

    $estimated_printing_start = $estimated_printing['start'];
    $estimated_printing_end = $estimated_printing['end'];

    if (isWeekend($estimated_printing_start)) {

        get_object_vars($estimated_printing_start);
        
        $estimated_printing_start_2 = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing_start->format('Y-m-d H:i:s'));
        //$estimated_printing_start_2 = new DateTime($estimated_printing_start->date);
        $estimated_printing_start_2->modify("next Monday");
        $estimated_printing_start_2 = addRangeHM($estimated_printing_start_2, array(
            'hours' => conf_workday()['from_hour'],
            'minutes' => conf_workday()['from_minute'] + 1
        ));

        $estimated_printing_end_2 = estimatedPrintingEnd($estimated_printing_start_2, $pending_part);

        $estimated_printing['start'] = $estimated_printing_start_2;
        $estimated_printing['end'] = $estimated_printing_end_2;
    }
    //Miro si el fin de la fecha estimada es fin de semana

    if (isWeekend($estimated_printing_end)) {
        $estimated_printing_start_2 = addRangeHM($estimated_printing_end->modify("next Monday"), array(
            'hours' => conf_workday()['from_hour'],
            'minutes' => conf_workday()['from_minute'] + 1
        ));

        $estimated_printing_end_2 = estimatedPrintingEnd($estimated_printing_start_2, $pending_part);

        $estimated_printing['start'] = $estimated_printing_start_2;
        $estimated_printing['end'] = $estimated_printing_end_2;
    }

    return true;
}

function removeOverlapAtNight(array &$estimated_printing, array $pending_part, string $start_date_time): bool
{
    //Comprobar que la nueva estimación no se solapa con la noche de incio o la de fin,
    //Si se solapa dependiendo de si es por delante o por detrás recalculamos la fecha estimada.
    $night = estimatedPrintingNightsFlex($estimated_printing);
    if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) && (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
        //echo "point";
    };

    if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) 
        || 
        (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
        $estimated_printing_aux = array();
        get_object_vars($estimated_printing['end']);
        $estimated_printing_aux['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
        //$estimated_printing_aux['start'] = new DateTime($estimated_printing['start']->date);
        
        if (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end'])){
            $estimated_printing_aux['start']->modify('+1 days');
        }

        $day_start = $estimated_printing_aux['start']->format('w');
        $estimated_printing_aux['start']->setTime(conf_working_days()[$day_start]['from']['hour'], conf_working_days()[$day_start]['from']['minute'] + conf_delays()['minutes_start']);
        //$estimated_printing_aux['start'] ->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']+1);

        $pending_part_aux = $pending_part;
        $pending_part_aux['estimated_printing_minutes'] ++;
        $estimated_printing_aux['end'] = estimatedPrintingEnd($estimated_printing_aux['start'], $pending_part_aux);

        $night = estimatedPrintingNights($estimated_printing_aux);

        if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing_aux['start'], $estimated_printing_aux['end'])) 
            || 
            (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing_aux['start'], $estimated_printing_aux['end']))) {

            $day_end = $estimated_printing['end']->format('w');
            if ($estimated_printing['end']->format('H') > 12) {
                $estimated_printing['end']->modify('+1 days');
            }
            $estimated_printing['end']->setTime(conf_working_days()[$day_end]['from']['hour'], conf_working_days()[$day_end]['from']['minute'] + conf_delays()['minutes_start']);
            $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
            
            if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) 
                || 
                (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {

                    
                    if ($estimated_printing['start'] ->format('H') < 12){
                        $estimated_printing['start'] ->setTime(conf_working_days()[$day_end]['from']['hour'], conf_working_days()[$day_end]['from']['minute'] + conf_delays()['minutes_start']);
                        $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
                        
                    }
            }
                
        } else {
            $estimated_printing = $estimated_printing_aux;
        }
    } else {
        //Si no se ha solapado
        return false;
    }
    return true;
}

function removeOverlapWeekendGap(&$gap_start, string $start_date_time): bool
{
    //Comprobar que el hueco no se solapa con la noche de incio o la de fin,
    //Si se solapa dependiendo de si es por delante o por detrás recalculamosel hueco de una manero u otra.
    $gap_start_aux = new DateTime($start_date_time);
    if ($gap_start->format('w') == 5 || $gap_start->format('w') == 6) {
        $gap_start_aux->next('Monday');
    }
    get_object_vars($gap_start);
    $gap_start = $gap_start_aux->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);

    return true;
}


function CleanDatetimeForBD(string $date_time):string{
    $dateTime = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time);
    $date = $dateTime->format('Y-m-d');
    $time = $dateTime->format('H:i:s');
    return $date .' ' . $time;
}

////////////////////////////////////////////  E N D  -  O V E R L A P ////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// G A P //////////////////////////////////////////////////////

//////////////////////////////////////////////// E N D  -  G A P /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


class DateTimeRange
{
    public $start, $end;

    function __construct(DateTime $start, DateTime $end)
    {
        $this->start = new DateTime($start->date);
        $this->end = new DateTime($end->date);
    }
}
