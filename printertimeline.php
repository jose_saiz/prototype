<?php

///////////////////////////////////////////////////////////// P R I N T E R S T I M E L I N E - C L A S S ////////////////////////////////////////////////////////////////
class PrinterTimeline extends ArrayObject
{
    public $id_printer, $code_printer;
    public $minutes_to_finish, $printing_minutes;
    public $roll_weight, $roll_replacement_datetime,$rolls_end;

    function __construct(int $id_printer, string $code_printer, float $roll_weight, DateTimeG $roll_replacement_datetime,array $rolls_end = NULL )
    {
        parent::__construct(array(), ArrayObject::ARRAY_AS_PROPS);
        $this->id_printer = $id_printer;
        $this->code_printer = $code_printer;
        $this->roll_weight = $roll_weight;
        $this->roll_replacement_datetime = $roll_replacement_datetime;
        $this->rolls_end = array();
        //array('from' => array('hour'=>'8','minute'=>'0')
        
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// Copy //////////////// //////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function copy(PrinterTimeline $printerTimeline):PrinterTimeline
    {
        $newPrinterTimeline = new PrinterTimeline ($printerTimeline->id_printer,
                                                   $printerTimeline->code_printer,
                                                   $printerTimeline->roll_weight,
                                                   $printerTimeline->roll_replacement_datetime,
                                                   $printerTimeline->rolls_end);
        foreach($printerTimeline as $slot){
            $new_slot = PrinterSlot::new($slot);
            $newPrinterTimeline->addSlotSimple($new_slot);
        }
        $newPrinterTimeline->uasort('startAscendingComparison');
        return $newPrinterTimeline;
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// chargePrinterTimeline //////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function chargePrinterTimeline(DateTimeG $start_date_time)
    {
        
        //Carga de BDD
        
        //Incializo PrinterMatrix
        $available_printer = $this;
        
        $sql = DataBase::chargePrinterTimeline($available_printer->id_printer);
        
        
        $printer_time_line = DataBase::getConnection()->query($sql);
        $first_iteration = true;
        
        foreach ($printer_time_line as $printer_slot) {
            
            
            
            if ($printer_slot['id_state'] == 9) {
                $slot_start_datetime = new DateTimeG($printer_slot['start_datetime']);
                //$slot_start_datetime = new DateTime($printer_slot['start_datetime']);
                
                
                if ($first_iteration) {
                    if ($slot_start_datetime < $start_date_time) {
                        //get_object_vars($start_datetime);
                        $slot_start_datetime = $start_date_time;
                    }
                    $first_iteration = false;
                }
                $slot_end_datetime = new DateTimeG($printer_slot['end_datetime']);
                //$slot_end_datetime = new DateTime($printer_slot['end_datetime']);
                get_object_vars($slot_start_datetime);
                get_object_vars($slot_end_datetime);
                
                if ($slot_end_datetime > $start_date_time) {
                    $printerSlot = new PrinterSlot(
                        $printer_slot['id'],
                        $printer_slot['id_order'],
                        $printer_slot['id_file'],
                        $printer_slot['file_name'],
                        $printer_slot['id_version'],
                        $printer_slot['id_product'],
                        $printer_slot['product_name'],
                        $printer_slot['id_final_product'],
                        //$printer_slot['start_datetime'],
                        //$slot_start_datetime->date,
                        $slot_start_datetime ->format('Y-m-d H:i:s'),
                        //$printer_slot['end_datetime'],
                        $slot_end_datetime ->format('Y-m-d H:i:s'),
                        $printer_slot['weight'],
                        $printer_slot['id_state'],
                        $printer_slot['initiated']);
                    $this->append($printerSlot);
                    
                    
                }
            }
        }
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// filamentCalculations ///////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function filamentCalculations (DateTime $start_date_time){
        //Caculo del filamento
        $printer_time_line = $this;
        $roll_replacement_datetime = $printer_time_line->roll_replacement_datetime;
        $slot_aux_end_datetime  =new DateTimeG( $start_date_time->format('Y-m-d H:i:s'));
        $available_filament = $printer_time_line->roll_weight;
        
        foreach ($printer_time_line as $printer_slot){
            
            $slot_start_datetime =new DateTimeG($printer_slot->start);
            $slot_end_datetime =new DateTimeG($printer_slot->end);
            
            
            
            //$slot_total_minutes= diffRangeHM($slot_start_datetime, $slot_end_datetime);
            
            //Si la fecha de sustitución está justo antes del slot actual
            if (($slot_aux_end_datetime < $roll_replacement_datetime)&&($slot_start_datetime >=$roll_replacement_datetime)){
                //var_dump($printer_slot);
                $available_filament -= $printer_slot->weight;
                //Si la fecha sustitución está dentro del slot actual
            }elseif(($slot_start_datetime <= $roll_replacement_datetime)&&($slot_end_datetime > $roll_replacement_datetime)){
                $slot_total_horasminutes= $slot_start_datetime->diffRangeHM($slot_end_datetime);
                $slot_new_roll_horasminutes = $roll_replacement_datetime->diffRangeHM($slot_end_datetime);
                $slot_total_minutes = $slot_total_horasminutes['hours']*60+$slot_total_horasminutes['minutes'];
                $slot_new_roll_minutes = $slot_new_roll_horasminutes['hours']*60+$slot_new_roll_horasminutes['minutes'];
                $percent = ($slot_new_roll_minutes *100)/$slot_total_minutes;
                
                $available_filament -= ($printer_slot->weight * $percent) /100;
            }else{
                //TODO Simepre pasa por aqui: parece que hay que tener en cuenta la condicion anterior: es nencesiro ananalizarlo mas a fondo
                $available_filament -= $printer_slot->weight;
            }
            
            $roll_end = array();
            //$available_filament -= $printer_slot->weight;
            //Si el filamento es negativo quiere decir que el filamento se va acabar antes de acabar la pieza
            if ($available_filament < 0){
                
                
                $percent = (abs($available_filament)*100)/$printer_slot->weight;
                //$percent = 100 - $percent;
                $slot_total_HM= $slot_start_datetime->diffRangeHM( $slot_end_datetime);
                $roll_end_minutes =  ((($slot_total_HM['hours']*60+ $slot_total_HM['minutes']) * $percent)/100);
                $slot_end_datetime->modify('-'.(int)$roll_end_minutes.' minutes');
                
                $roll_end['datetime'] = $slot_end_datetime;
                $roll_end['pending_part'] = $printer_slot;
                $roll_end['pending_filament'] = -$available_filament;
                
                //Comprobar que la fecha no es muy en el 
                
                array_push($printer_time_line->rolls_end,$roll_end);
                
                $roll_replacement_datetime = $slot_end_datetime;
                //El filamento disponible es el del rollo nuevo menos lo que gasta la pieza actual
                $available_filament = conf_default_roll_weight() + $available_filament; //OJO que como es negativo el filamento disponible se resta
                
            }elseif($available_filament == 0){
                
                $roll_end['datetime'] = $slot_end_datetime;
                
                array_push($printer_time_line->rolls_end,$roll_end);
                
                $roll_replacement_datetime = $slot_start_datetime;
                //El filamento disponible es el del rollo nuevo menos lo que gasta la pieza actual
                $available_filament = conf_default_roll_weight() + $available_filament; //OJO que como es negativo se resta
                
            }
            $slot_aux_end_datetime =new DateTimeG( $printer_slot->end);
        }
        $roll_replacement_datetime = $printer_time_line->rolls_end;
    
    //var_dump($printer_time_line->roll_end_datetime);
    }
    
    
    

    public function addSlot($id_part = NULL, $id_order = NULL, $id_file = NULL, $file_name = NULL, $id_version= NULL, $product = NULL, $product_name = NULL, $final_product = NULL, 
                            $estimated_printing_start = NULL, $estimated_printing_end = NULL, $part_weight = NULL, $state = NULL, $initiated = NULL): bool
    {
        $printerSlot = new PrinterSlot($id_part, $id_order, $id_file, $file_name, $id_version, $product, $product_name, $final_product, $estimated_printing_start, $estimated_printing_end, $part_weight, $state,$initiated);

        $this->append($printerSlot);

        //TODO añadir a la bdd

        return true;
    }
    
    public function addSlotSimple(PrinterSlot $printerSlot): bool
    {
        
        $this->append($printerSlot);
        
        //TODO añadir a la bdd
        
        return true;
    }

    public function addSlotAndSortAsc(array $estimated_printing, array $pending_part): bool
    {
        get_object_vars($estimated_printing['start']);
        get_object_vars($estimated_printing['end']);
        
        $printerSlot = new PrinterSlot( $pending_part['id'], 
                                        $pending_part['id_order'], 
                                        $pending_part['id_file'], 
                                        $pending_part['file_name'], 
                                        $pending_part['id_version'],
                                        $pending_part['id_product'],
                                        $pending_part['product_name'],
                                        $pending_part['id_final_product'], 
                                        $estimated_printing['start']->format('Y-m-d H:i:s'), 
                                        $estimated_printing['end']->format('Y-m-d H:i:s'), 
                                        $pending_part['weight'], 
                                        '8', 
                                        $pending_part['initiated']);
        $this->append($printerSlot);
        $this->uasort('startAscendingComparison');

        return true;
    }
    
    
    public function SlotSortAsc(): bool
    {

        $this->uasort('startAscendingComparison');
        
        return true;
    }
    
    
    public function getMinutesToFinish(): int
    {}
    
    
    public function getPartialMinutes(DateTime $start_date_time,DateTime $finish_date_time): int
    {
        $arrayIterator = $this->getIterator();
        $total_minutes = 0;
        $gap_start= $start_date_time;
        
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            $gap_minutes = rangeTotalM($gap_start, $gap_end);
            
            $start_date = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            $end_date = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            $slot_minutes = rangeTotalM($start_date, $end_date);
            
            
            if ( $finish_date_time < $end_date){
                $slot_partial_minutes = rangeTotalM($start_date, $finish_date_time);
                $total_minutes +=  $gap_minutes + $slot_partial_minutes;
                break;
            }else{
                $total_minutes += $gap_minutes + $slot_minutes;
            }
            
            //$total_minutes += $gap_minutes + $slot_minutes;
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            
            $arrayIterator->next();
            
        }
        return $total_minutes;
        
    }
    
    
    
    public function getTotalMinutes(DateTimeG $start_date_time): int
    {
        $arrayIterator = $this->getIterator();
        $total_minutes = 0;
        $gap_start= $start_date_time;
        
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            
            $gap_end =new DateTimeG($printerSlot->start);
            //$30-03 gap_minutes = rangeTotalM($gap_start, $gap_end);
            $gap_minutes = $gap_start->rangeTotalM($gap_end);
            
            $start_date =new DateTimeG($printerSlot->start);
            $end_date =new DateTimeG($printerSlot->end);
            //30-03 $slot_minutes = rangeTotalM($start_date, $end_date);
            $slot_minutes = $start_date->rangeTotalM($end_date);
            
            $total_minutes += $gap_minutes + $slot_minutes;
            $gap_start =new DateTimeG($printerSlot->end);
            
            $arrayIterator->next();
            
        }
        return $total_minutes;
        
    }
    
    
    
    public function getRange(): array
    {
        $this->uasort('startAscendingComparison');
        
        
        $arrayIterator = $this->getIterator();
        if  ($arrayIterator->valid()) {
            $slot_start = $arrayIterator->current();
        }
        $arrayIterator->next();
        
        While ($arrayIterator->valid()) {
            $slot_end = $arrayIterator->current();
            $arrayIterator->next();
        }
        if (isset($slot_start->start)){
            return array('start' => $slot_start->start,'end' => $slot_end->end);
        }
        return array('start' => '','end' => '');
    }
    
    public function getAssignaments(): array
    {
        $cont = array();
        $cont['created'] = 0;
        $cont['assigned'] = 0;
        $cont['separated'] = 0;
        $this->uasort('startAscendingComparison');
        
        
        $arrayIterator = $this->getIterator();
        
        While ($arrayIterator->valid()) {
            $slot = $arrayIterator->current();
            
            //if ($slot->state == 9) && ($slot->initiated ==1); 
            if ($slot->state == 9) {
                $cont['created']++;
            }elseif ($slot->state == 8) {
                $cont['assigned']++;
            }elseif ($slot->state == 7) {
                $cont['separated']++;
            }
            
            
            $arrayIterator->next();
        }
        
        return $cont;
    }
    public function getPrintingMinutes(DateTimeG $start_date_time): int
    {
        $arrayIterator = $this->getIterator();
        $total_minutes = 0;
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            
            $start_date =new DateTimeG($printerSlot->start);
            if ($start_date < $start_date_time) {
                $start_date = $start_date_time;
            }
            
            $end_date =new DateTimeG($printerSlot->end);
            
            //31-03 $minutes = rangeTotalM($start_date, $end_date);
            $minutes = $start_date-> rangeTotalM($end_date);

            $total_minutes += $minutes;

            $arrayIterator->next();
        }
        return $total_minutes;
    }
    
    public function getGapsMinutes(DateTimeG $start_date_time): int
    {
        $gap_start= $start_date_time;
        
        $arrayIterator = $this->getIterator();
        $total_minutes = 0;
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            
            $gap_end =new DateTimeG($printerSlot->start);
            
            if ($gap_end < $start_date_time){
                $gap_end = $start_date_time;
            }
            
            //$gap_end =  new DateTime($printerSlot->start);
            
            //30-03 $minutes = rangeTotalM($gap_start, $gap_end);
            $minutes = $gap_start->rangeTotalM($gap_end);
            
            $total_minutes += $minutes;
            $gap_start =new DateTimeG($printerSlot->end);
            //$gap_start= new DateTime($printerSlot->end);
            
            $arrayIterator->next();
        }
        return $total_minutes;
    }
    
    
    public function getGapWithoutNight(DateTime $start_date_time,array $pendig_part): array
    {
        $result = array();
        $result['minutes'] = 0;
        $gap_start= DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time->format('Y-m-d H:i:s'));
        
        $day_start = $gap_start->format('w');
        $gap_start->setTime(conf_working_days()[$day_start]['from']['hour'], conf_working_days()[$day_start]['from']['minute']);
        
        $pending_part_minutes = $pendig_part['estimated_printing_hours']*60+$pendig_part['estimated_printing_minutes'];
        
        $arrayIterator = $this->getIterator();
        
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            //gap_end =  new DateTime($printerSlot->start);
            
            $minutes = rangeTotalM($gap_start, $gap_end);
            if ((!isOverlapAtNight($gap_start)) && ($pending_part_minutes <= $minutes )){
                $result['start'] = $gap_start;
                $result['end'] = $gap_end;
                $result['gap_minutes'] =  $minutes;
                $result['pending_part_minutes'] = $pending_part_minutes;
                $result['slot'] = $printerSlot;
                break;
                
            }
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            //$gap_start= new DateTime($printerSlot->end);
            
            $arrayIterator->next();
        }
        return $result;
    }
    
    
    
    
    
    
    
    
    
    
    
    public function getBiggestGap(DateTime $start_date_time): array
    {
        $result = array();
        $result['minutes'] = 0;
        $gap_start= DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time->format('Y-m-d H:i:s'));
        
        $day_start = $gap_start->format('w');
        $gap_start->setTime(conf_working_days()[$day_start]['from']['hour'], conf_working_days()[$day_start]['from']['minute']);
        
        $max_minutes = 0;
        
        $arrayIterator = $this->getIterator();
        
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            //gap_end =  new DateTime($printerSlot->start);

            $minutes = rangeTotalM($gap_start, $gap_end);
            if ((!isOverlapAtNight($gap_start)) && ($max_minutes <= $minutes )){
                $result['start'] = $gap_start;
                $result['end'] = $gap_end;
                $result['minutes'] = $max_minutes = $minutes;
                $result['slot'] = $printerSlot;
                
            }
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            //$gap_start= new DateTime($printerSlot->end);
            
            $arrayIterator->next();
        }
        return $result;
    }
    
    public function getBiggestGapWithoutNight(DateTime $start_date_time): array
    {
        $result = array();
        $result['minutes'] = 0;
        $gap_start= DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time->format('Y-m-d H:i:s'));
        
        $day_start = $gap_start->format('w');
        $gap_start->setTime(conf_working_days()[$day_start]['from']['hour'], conf_working_days()[$day_start]['from']['minute']);
        
        $max_minutes = 0;
        
        $arrayIterator = $this->getIterator();
        
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            //gap_end =  new DateTime($printerSlot->start);
            
            $minutes = rangeTotalM($gap_start, $gap_end);
            if ((!isOverlapAtNight($gap_start)) && ($max_minutes <= $minutes )){
                $result['start'] = $gap_start;
                $result['end'] = $gap_end;
                $result['minutes'] = $max_minutes = $minutes;
                $result['slot'] = $printerSlot;
                
            }
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            //$gap_start= new DateTime($printerSlot->end);
            
            $arrayIterator->next();
        }
        return $result;
    }
    
    
    public function getBiggestGap2(DateTime $start_date_time): array
    {
        $result = array();
        $result['minutes'] = 0;
        $gap_start= DateTime::CreateFromFormat('Y-m-d H:i:s',$start_date_time->format('Y-m-d H:i:s'));
        
        $day_start = $gap_start->format('w');
        
        //TODO la hora del gap solo se tiene que actualizar: si la fecha actual es mayor que la de inicio de jornada.
        $gap_start  ->setTime(conf_working_days()[$day_start]['from']['hour'], conf_working_days()[$day_start]['from']['minute'])
                    ->modify("+" . conf_delays()['minutes_start'] . " minutes");
        
        $max_minutes = 0;
        
        $arrayIterator = $this->getIterator();
        
        While ($arrayIterator->valid()) {
            $printerSlot = $arrayIterator->current();
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            //gap_end =  new DateTime($printerSlot->start);
            
            $minutes = rangeTotalM($gap_start, $gap_end);
            if ((!isOverlapAtNight($gap_start)) && ($max_minutes <= $minutes )){
                $result['start'] = $gap_start;
                $result['end'] = $gap_end;
                $result['minutes'] = $max_minutes = $minutes;
                $result['slot'] = $printerSlot;
                
            }
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            //$gap_start= new DateTime($printerSlot->end);
            
            $arrayIterator->next();
        }
        return $result;
    }
    
    public function percent($start_datetime): float
    {
        $percent = 0;
        if ($this->getPrintingMinutes($start_datetime)!=0){
            $percent = (100-((100*$this->getGapsMinutes($start_datetime)/$this->getPrintingMinutes($start_datetime))));
        }
        return $percent;
    }
    
    
    public function countOverlaps($start_date_time):int
    {
        //Buscar solapamientos
        $count_overlaps= 0;
        $printer_slot_aux = new PrinterSlot("","","", "", "", "", $start_date_time->format('Y-m-d H:i:s'), $start_date_time->format('Y-m-d H:i:s'), "", "","");
        $first_iteration = true;
            $this->uasort('startAscendingComparison');
            //$printerTimeLine->uasort('startAscendingComparison');
            //echo "Solapamientos-".$printerTimeLine->code_printer.": <br>";
            foreach ($this as $printer_slot){
                if (
                    (is_overlap_or_wrap(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end))
                        )
                    &&
                    (!$first_iteration)
                    )
                {
                        
                        $count_overlaps ++;
                        
                }else{
                    $first_iteration = false;
                }
                $printer_slot_aux = $printer_slot;
            }
            //echo '<br>';
        return $count_overlaps;
    }
    public function getOverlaps($start_date_time):array
    {
        //Buscar solapamientos
        $overlaps = array();
        $printer_slot_aux = new PrinterSlot("", "", "", "", "",$start_date_time->format('Y-m-d H:i:s'), $start_date_time->format('Y-m-d H:i:s'), "", "","");
        $first_iteration = true;
        $this->uasort('startAscendingComparison');
        //$printerTimeLine->uasort('startAscendingComparison');
        //echo "Solapamientos-".$printerTimeLine->code_printer.": <br>";
        foreach ($this as $printer_slot){
            if (
                (is_overlap_or_wrap(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                    DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end),
                    DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start),
                    DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end))
                    )
                &&
                (!$first_iteration)
                )
            {
                $diff = diffRangeHM( DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                    DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end));
                echo '-------------------'. $printer_slot->part.':'.$printer_slot->file.' - '. $diff['hours'].':'.$diff['minutes'].'<br/>';
                $overlaps[$printer_slot->part]= $printer_slot->file;
                
                
                
            }else{
                $first_iteration = false;
            }
            $printer_slot_aux = $printer_slot;
        }
        //echo '<br>';
        return $overlaps;
    }
    
    public static function swapNightOverlaps(DateTime $start_date_time,PrinterTimeline $printerTimeline)
    {
        $printerTimeline->uasort('startAscendingComparison');
        $printerTimeLineNew = PrinterTimeline::copy($printerTimeline);
        

        $printerTimeLineIterator = $printerTimeLineNew->getIterator();
        $printerAux = $printerTimeLineNew->getArrayCopy();
        $end_key = end($printerAux);

        $printerSlotPrev = new PrinterSlot("","","", "", "", "", "", "", "","");
        $key_prev = 0;
        if ($printerTimeLineIterator->valid()){
            $printerSlotPrev = $printerTimeLineIterator->current();
            $key_prev =  $printerTimeLineIterator->key();
        }
        $first_iteration = true;
        
        $max_gap_start = createDateTime($start_date_time->format('Y-m-d H:i:s'));
        $max_gap_end =  createDateTime($start_date_time->format('Y-m-d H:i:s'));
        $minutes_max_gap =0;
        $slot_max_gap = new PrinterSlot("","","", "", "", "", "", "", "","");
        While ($printerTimeLineIterator->valid()) {
            
            $printerSlot = $printerTimeLineIterator->current();

            
            if ($first_iteration){
                //echo "point";
            }
            $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start);
            $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
            $slot_start_prev = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotPrev->start);
            $slot_end_prev = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotPrev->end);
            $minutes = diffMinutes($slot_start, $slot_end);
            $minutes_prev = diffMinutes($slot_start_prev, $slot_end_prev);

            
            
            
            if ($first_iteration){
                //echo "point";
            }
            if ($minutes_max_gap < diffMinutes($max_gap_start,$max_gap_end)){
            //Voy buscando el hueco más grande y guardo su información
                
                $slot_max_gap = $printerSlotPrev;
                $key_slot_max_gap =  $key_prev;
                $slot_max_gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$slot_max_gap->start);
                $slot_max_gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$slot_max_gap->end);
                $minutes_slot_max_gap = diffMinutes($slot_max_gap_start,$slot_max_gap_end);
                setDateTime($max_gap_start,$printerSlotPrev->end);
                setDateTime($max_gap_end,$printerSlot->start );
                $minutes_max_gap = diffMinutes($max_gap_start,$max_gap_end);
            }
            
            
            
            if (($printerSlot->state == 8) 
                && 
                (isOverlapAtNight($slot_start))
                &&
                ($minutes_prev >$minutes)
                //true
               ){
            //Si se solapa por la noche: Swap con el Anterior, siempre y cuando se pueda
            //Si el Anterior es mayor que el actual
                    //Se puede si al desplazarlo al inicio del Anterior no se solapa al final con la noche
                    
                    $slot_end_new = estimatedPrintingEndSlot($slot_start_prev, $printerSlot);
                    
                    if (!isOverlapAtNight($slot_end_new)){
                           $printerSlot->start = $slot_start_prev->format('Y-m-d H:i:s');
                           $printerSlot->end = $slot_end_new ->format('Y-m-d H:i:s');
                           
                           $printerSlotPrev->start = $slot_end_new->modify("+" . conf_delays()['minutes_start']." minutes")->format('Y-m-d H:i:s');
                           $printerSlotPrev->end = $slot_end_new->modify("+" .$minutes_prev." minutes")->format('Y-m-d H:i:s');
                           
                    }
            }elseif(($printerSlot->state == 8)
                &&
                (isOverlapAtNight($slot_start))
                &&
                //($minutes_prev < 60)
                (isSizeHours('smallest', intdiv($minutes_prev, 60)))
                &&
                //($minutes > 180)https://www.php.net/manual/es/arrayiterator.key.php
                //(isSizeHours('median', intdiv($minutes, 60)))
                true
                ){
                    $slot_end_new = estimatedPrintingEndSlot($slot_start_prev, $printerSlot);
                    
                    $printerSlot->start = $slot_start_prev->format('Y-m-d H:i:s');
                    $printerSlot->end = $slot_end_new ->format('Y-m-d H:i:s');
                    unset($printerTimeLineNew[$key_prev]);
                    //unset ($printerSlotPrev);
                
            }elseif(($printerTimeLineIterator->key() === $end_key)
                &&
                (isOverlapAtNight($slot_start))
                &&
                ($minutes_slot_max_gap < $minutes)
                &&
                ($minutes < $minutes_max_gap + $minutes_slot_max_gap))
                //Si es el último elemento el que está solapado por la noche
                //vamos a intertar intercambiarlo con el slot que está delante del hueco más grande.
                //Como lo que queremos es reducir ese hueco, la pieza tiene que ser más grande, pero tiene que caber.
                
            {
                $printerSlot->start = $slot_max_gap->start;
                $printerSlot->end = $slot_max_gap->end;
                unset($printerTimeLineNew[$key_slot_max_gap]);
            }
            
            $first_iteration = false;
            
            $printerSlotPrev = $printerSlot;
            $key_prev =  $printerTimeLineIterator->key();
            $printerTimeLineIterator->next();
        }//END While ($thisIterator->valid()) {
        //Comprobar si la nueva linea de tiempo ha mejorado el rendimiento y si es así sustituirla.
        
            
        return $printerTimeLineNew;
    }
    
    
    public static function reassignNightOverlaps(DateTime $start_date_time,PrinterTimeline $printerTimeline)
    {
        $delete_keys = array();
        $printerTimeline->uasort('startAscendingComparison');
        $printerTimeLineNew = PrinterTimeline::copy($printerTimeline);
        
        //if ($this->percent($start_date_time)<90){
        if (true){
            
            
            $night_parts = array();
            foreach ($printerTimeLineNew as $key =>$printer_slot){
                $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                if (($printer_slot->state == 8) && (isOverlapAtNight($slot_start))){
                    /*
                     $overlaped_slot = new PrinterSlot(  $printer_slot->part, $printer_slot->order, $printer_slot->file, $printer_slot->version,
                     $printer_slot->final_product, $printer_slot->start, $printer_slot->end,
                     $printer_slot->weight, $printer_slot->state,$printer_slot->initiated);
                     */
                    
                    $overlaped_slot = $printer_slot->toPendingPart();
                    $night_parts[$key] =  $overlaped_slot;
                    if ($printer_slot->part== 249){
                        //echo "point";
                    }
                    unset($printerTimeLineNew[$key]);
                    
                }
            }
            $gap_start = createDateTime($start_date_time->format('Y-m-d H:i:s'));
            $gap_end =  createDateTime($start_date_time->format('Y-m-d H:i:s'));
            $count = 0;
            
            
            if (isset($night_parts) && (count ($night_parts)>0)){
                //$night_parts->uasort('sort_by_hours');

                $printerTimeLineIterator = $printerTimeLineNew->getIterator();
                $assigned_night_overlap = false;
                $printerSlotAux = new PrinterSlot("","","", "", "", "", "", "", "","");
                if ($printerTimeLineIterator->valid()){
                    $printerSlotAux = $printerTimeLineIterator->current();
                }
                $first_iteration = true;
                While ($printerTimeLineIterator->valid()) {
                    
                    $printerSlot = $printerTimeLineIterator->current();
                    if ($first_iteration){
                        //echo "point";
                    }
                    $first_iteration = false;
                    setDateTime($gap_end,$printerSlot->start );
                    if (!$assigned_night_overlap){
                        //Si no se ha asignado el slot que se había solapado por la noche, hay que mirar si lo podemos asignar en este hueco
                        if (!isOverlapAtNight($gap_start)){
                            $assigned_keys = array();
                            $night_part_id = 0;
                            usort($night_parts, 'sort_by_hours');
                            foreach ($night_parts as $key => $night_part ){
                                
                                //if{
                                if (
                                    (   //La nueva pieza no acabará por la noche.
                                        diffHours($gap_start,estimatedPrintingNightFlex($gap_end)['start']->modify("+" . conf_delays()['minutes_start'] . " minutes"))
                                        >
                                        $night_part ['estimated_printing_hours']
                                        )
                                    &&
                                    (  //El hueco es los suficientemente grande, para la pieza que queremos utilizar.
                                        //No tiene que ser más grande que la pieza, ya que vamos a desplazar las que hay despues
                                        //Pero si es muy pequeño, al poner la nueva pieza, el resto se desplazarán mucho a la derecha y  nos podremos ubicarlas.
                                        diffHours($gap_start,$gap_end)
                                        >
                                        $night_part ['estimated_printing_hours']/3
                                        )
                                    )
                                    
                                {
                                    
                                    $newPrinterSlot = PrinterSlot::fromPendingPart($night_part ,$gap_start->modify("+" . conf_delays()['minutes_start'] . " minutes")->format('Y-m-d H:i:s'));
                                    $end_new = createDateTime($newPrinterSlot->end);
                                    if (!isOverlapAtNight($end_new)){
                                        $printerTimeLineNew->addSlotSimple($newPrinterSlot);
                                        unset($night_parts[$key]);
                                        array_push($assigned_keys,$key);
                                        $night_part_id = $night_part['id'];
                                        
                                        $start = $end_new->modify("+" . conf_delays()['minutes_start'] . " minutes");
                                        $end = estimatedPrintingEndSlot($start,$printerSlot);
                                        $printerSlot->start = $start->format('Y-m-d H:i:s');
                                        $printerSlot->end = $end->format('Y-m-d H:i:s');
                                        
                                        
                                        //$this->uasort('startAscendingComparison');
                                        setDateTime($gap_start,$printerSlot->end);
                                        
                                        $assigned_night_overlap = true;
                                        
                                        break;
                                        /*
                                         $printerSlotAux = $printerSlot;
                                         $thisIterator->next();
                                         */
                                        
                                    }
                                }elseif ($assigned_night_overlap){
                                    //echo "point";
                                    setDateTime($gap_start,$printerSlot->end);
                                }else{
                                    setDateTime($gap_start,$printerSlot->end);
                                }
                            }//END foreach ($night_parts as $key => $night_part ){
                        }else{
                            setDateTime($gap_start,$printerSlot->end);
                        }
                    }else{
                        //Si ya hemos asignado el slot que se solapaba por la noche y el siguienterecolocamos el resto, comp
                        if ($night_part_id != $printerSlot->part){
                            //Si no es el slot que hemos movido entonces lo recolocamos
                            $start = $gap_start->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            if (isOverlapAtNight($start)){
                                $start = estimatedPrintingNightFlex($start)['end']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            }
                            
                            $end = estimatedPrintingEndSlot($start,$printerSlot);
                            $printerSlot->start = $start->format('Y-m-d H:i:s');
                            $printerSlot->end = $end->format('Y-m-d H:i:s');
                            
                            setDateTime($gap_start,$printerSlot->end);
                        }
                        
                    }
                    
                    //setDateTime($gap_start,$printerSlot->end);
                    $printerSlotAux = $printerSlot;
                    $printerTimeLineIterator->next();
                }//END While ($thisIterator->valid()) {
                //Comprobar si la nueva linea de tiempo ha mejorado el rendimiento y si es así sustituirla.

            }
        }
        return $printerTimeLineNew;
    }
    
    
    public static function reassignNightOverlaps2(DateTime $start_date_time,PrinterTimeline $printerTimeline)
    {
        if ($printerTimeline->id_printer == 9){
            //echo "point";
        }
        $delete_keys = array();
        $printerTimeline->uasort('startAscendingComparison');
        $printerTimeLineNew = PrinterTimeline::copy($printerTimeline);
        
        //if ($this->percent($start_date_time)<90){
        if (true){
            
            
            $night_parts = array();
            foreach ($printerTimeLineNew as $key =>$printer_slot){
                $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                if (($printer_slot->state == 8) && (isOverlapAtNight($slot_start))){
                    /*
                     $overlaped_slot = new PrinterSlot(  $printer_slot->part, $printer_slot->order, $printer_slot->file, $printer_slot->version,
                     $printer_slot->final_product, $printer_slot->start, $printer_slot->end,
                     $printer_slot->weight, $printer_slot->state,$printer_slot->initiated);
                     */
                    
                    $overlaped_slot = $printer_slot->toPendingPart();
                    $night_parts[$key] =  $overlaped_slot;
                    if ($printer_slot->part== 249){
                        //echo "point";
                    }
                    unset($printerTimeLineNew[$key]);
                    
                }
            }
            $gap_start = createDateTime($start_date_time->format('Y-m-d H:i:s'));
            $gap_end =  createDateTime($start_date_time->format('Y-m-d H:i:s'));
            $count = 0;
            
            
            if (isset($night_parts) && (count ($night_parts)>0)){
                //$night_parts->uasort('sort_by_hours');
                
                $printerTimeLineIterator = $printerTimeLineNew->getIterator();
                $assigned_night_overlap = false;
                $printerSlotAux = new PrinterSlot("","","", "", "", "", "", "", "","");
                if ($printerTimeLineIterator->valid()){
                    $printerSlotAux = $printerTimeLineIterator->current();
                }
                $first_iteration = true;
                While ($printerTimeLineIterator->valid()) {
                    
                    $printerSlot = $printerTimeLineIterator->current();
                    if ($first_iteration){
                        //echo "point";
                    }
                    $first_iteration = false;
                    setDateTime($gap_end,$printerSlot->start );
                    if (!$assigned_night_overlap){
                        //Si no se ha asignado el slot que se había solapado por la noche, hay que mirar si lo podemos asignar en este hueco
                        if (!isOverlapAtNight($gap_start)){
                            $assigned_keys = array();
                            $night_part_id = 0;
                            usort($night_parts, 'sort_by_hours');
                            foreach ($night_parts as $key => $night_part ){
                                
                                //if{
                                if (
                                    (   //La nueva pieza no acabará por la noche.
                                        diffHours($gap_start,estimatedPrintingNightFlex($gap_end)['start']->modify("+" . conf_delays()['minutes_start'] . " minutes"))
                                        >
                                        $night_part ['estimated_printing_hours']
                                        )
                                    &&
                                    (  //El hueco es los suficientemente grande, para la pieza que queremos utilizar.
                                        //No tiene que ser más grande que la pieza, ya que vamos a desplazar las que hay despues
                                        //Pero si es muy pequeño, al poner la nueva pieza, el resto se desplazarán mucho a la derecha y  nos podremos ubicarlas.
                                        diffHours($gap_start,$gap_end)
                                        >
                                        $night_part ['estimated_printing_hours']/2
                                        )
                                    )
                                    
                                {
                                    
                                    $newPrinterSlot = PrinterSlot::fromPendingPart($night_part ,$gap_start->modify("+" . conf_delays()['minutes_start'] . " minutes")->format('Y-m-d H:i:s'));
                                    $end_new = createDateTime($newPrinterSlot->end);
                                    if (!isOverlapAtNight($end_new)){
                                        if ($count > 0){
                                            $printerTimeLineNew->addSlotSimple($newPrinterSlot);
                                            unset($night_parts[$key]);
                                            array_push($assigned_keys,$key);
                                            $night_part_id = $night_part['id'];
                                            
                                            
                                            /*
                                             $start_aux = $end_new->modify("+" . conf_delays()['minutes_start'] . " minutes");
                                             $end_aux = estimatedPrintingEndSlot($start_aux,$printerSlotAux);
                                             $printerSlotAux->start = $start_aux->format('Y-m-d H:i:s');
                                             $printerSlotAux->end = $end_aux->format('Y-m-d H:i:s');
                                             
                                             
                                             $start = $end_aux->modify("+" . conf_delays()['minutes_start'] . " minutes");
                                             $end = estimatedPrintingEndSlot($start,$printerSlot);
                                             $printerSlot->start = $start->format('Y-m-d H:i:s');
                                             $printerSlot->end = $end->format('Y-m-d H:i:s');
                                             */
                                            
                                            
                                            $start = $end_new->modify("+" . conf_delays()['minutes_start'] . " minutes");
                                            $end = estimatedPrintingEndSlot($start,$printerSlot);
                                            $printerSlot->start = $start->format('Y-m-d H:i:s');
                                            $printerSlot->end = $end->format('Y-m-d H:i:s');
                                            
                                            
                                            //$this->uasort('startAscendingComparison');
                                            setDateTime($gap_start,$printerSlot->end);
                                            
                                            $assigned_night_overlap = true;
                                            
                                            break;
                                        }else{
                                            setDateTime($gap_start,$printerSlot->end);
                                            $count++;
                                        }
                                        
                                        /*
                                         $printerSlotAux = $printerSlot;
                                         $thisIterator->next();
                                         */
                                        
                                    }
                                }elseif ($assigned_night_overlap){
                                    //echo "point";
                                    setDateTime($gap_start,$printerSlot->end);
                                }else{
                                    setDateTime($gap_start,$printerSlot->end);
                                }
                            }//END foreach ($night_parts as $key => $night_part ){
                        }else{
                            setDateTime($gap_start,$printerSlot->end);
                        }
                    }else{
                        //Si ya hemos asignado el slot que se solapaba por la noche y el siguienterecolocamos el resto, comp
                        if ($night_part_id != $printerSlot->part){
                            //Si no es el slot que hemos movido entonces lo recolocamos
                            $start = $gap_start->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            if (isOverlapAtNight($start)){
                                $start = estimatedPrintingNightFlex($start)['end']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            }
                            
                            $end = estimatedPrintingEndSlot($start,$printerSlot);
                            $printerSlot->start = $start->format('Y-m-d H:i:s');
                            $printerSlot->end = $end->format('Y-m-d H:i:s');
                            
                            setDateTime($gap_start,$printerSlot->end);
                        }
                        
                    }
                    
                    //setDateTime($gap_start,$printerSlot->end);
                    $printerSlotAux = $printerSlot;
                    $printerTimeLineIterator->next();
                }//END While ($thisIterator->valid()) {
                //Comprobar si la nueva linea de tiempo ha mejorado el rendimiento y si es así sustituirla.
                
            }
        }
        return $printerTimeLineNew;
    }
    
}
