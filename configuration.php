<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('Europe/Madrid');
setlocale(LC_ALL,"es_ES");

define ('URL',"http://localhost:9000");

define ('WORKDAY_TYPE',array('normal'=> 0 ,'no_weekends'=> 1,'no_stops'=>2));

//define ('conf_parts_range()',array('biggest' => array(50,80),'big'=>array(30,50),'median'=>array(20,30),'small'=>array(6,20),'smallest'=> array(0,6)));
//define ('conf_parts_range()',array('biggest' => array(42,80),'big'=>array(30,42),'median'=>array(25,30),'small'=>array(9,25),'smallest'=> array(0,9)));
//define ('conf_parts_range()',array('biggest' => array(42,80),'big'=>array(30,42),'median'=>array(9,30),'small'=>array(8,8),'smallest'=> array(0,7)));

define ('PARTS_RANGES',array('biggest' => array(38,80),'big'=>array(19,38),'median'=>array(7,19),'small'=>array(7,3),'smallest'=> array(0,3)));


//define ('PARTS_RANGES',array('biggest' => array(80,180),'big'=>array(30,79),'median'=>array(3,30),'small'=>array(2,2),'smallest'=> array(0,1)));
//define ('PARTS_RANGES',array('biggest' => array(82,180),'big'=>array(80,81),'median'=>array(3,79),'small'=>array(2,2),'smallest'=> array(0,1)));
//define ('conf_parts_range()',array('biggest' => array(42,80),'big'=>array(9,41),'median'=>array(8,8),'small'=>array(7,7),'smallest'=> array(0,6)));

define ('WORKDAY',array('from_hour'=>'08','from_minute'=>'00' ,'to_hour'=>'21','to_minute'=>'30'));
//define ('WORKDAY',array('from_hour'=>'06','from_minute'=>'59' ,'to_hour'=>'17','to_minute'=>'50'));

define ('WEEK_WORKING_DAYS',array('Monday','Tuesday','Wednesday','Thursday','Friday'));
//define ('WEEK_WORKING_DAYS',array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'));

define ('WORKING_DAYS',array( //'WORKING_DAYS[2]['from']['hour'] => Wednesday - from - hour = 8
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')), //Sunday = 0
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')), //'Monday =1
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')), //Tuesday = 2
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')), //Wednesday = 3
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')), //Thursday = 4
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')), //Friday = 5
    array('from' => array('hour'=>'08','minute'=>'00'),'to'=>array('hour'=>'21','minute'=>'30')) //Saturday = 6
));


define ('PARTS_COLORS',array(
    323=>array(array(192,0,0),      array(255,255,255)),
    327=>array(array(151,71,6),     array(255,255,255)),
    294=>array(array(226,107,10),   array(0,0,0)),
    332=>array(array(229,214,122),  array(0,0,0)),
    332=>array(array(229,214,122),  array(0,0,0)),
    322=>array(array(36,64,98),     array(255,255,255)),
    314=>array(array(0,176,240),    array(0,0,0)),
    283=>array(array(146,205,220),  array(0,0,0)),
    331=>array(array(218,243,218),  array(0,0,0)),
    320=>array(array(245,245,255),  array(0,0,0)),
    276=>array(array(0,176,80),     array(0,0,0)),
    303=>array(array(146,208,80),   array(0,0,0)),
    338=>array(array(196,215,155),  array(0,0,0)),
    341=>array(array(235,255,222),  array(0,0,0)),
    
    334=>array(array(0,215,155),    array(0,0,0)),
    335=>array(array(50,215,155),   array(0,0,0)),
    336=>array(array(100,215,155),  array(0,0,0)),
    348=>array(array(50,50,155),  array(255,255,255)),
    408=>array(array(0,80,215),    array(0,0,0)),
    'K0327'=>array(array(199,116,36),      array(0,0,0)),
    'Kv3-0294'=>array(array(0,150,215),   array(0,0,0)),
    'P10001'=>array(array(0,0,215),       array(255,255,255)),
    'P10002'=>array(array(215,215,50),      array(0,0,0)),
    'P10009'=>array(array(150,20,20),     array(255,255,255)),
    
));


define ('HOLLYDAYS','VOID');
define ('DELAYS',array('minutes_start'=>10,'minutes_end'=>5));


define ('DEFAULT_ROLL_WEIGHT',2159);

define('ID_NEIGHBORHOOD_FACTORY',1);

define ('DAYS_OUT_RANGE_SLOT', 2);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// D A T A B A S E   C H A R G E  C O N F I G U R A T I O N //////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getPartsColors(){
    
    $parts_colors =  array();
    $sql = sprintf('SELECT  * FROM    file ORDER BY id_file, id_version');
    $parts = DataBase::getConnection()->query($sql);
    //Cargo la matriz de colores de los slots
    foreach ($parts as $part) {
        if ($part['color']){
            $rgb = explode(',',$part['color']);
            $rgb_background = explode(',',$part['color_background']);
            $parts_colors[$part['id_file']][0][0] = $rgb[0];
            $parts_colors[$part['id_file']][0][1] = $rgb[1];
            $parts_colors[$part['id_file']][0][2] = $rgb[2];
            
            $parts_colors[$part['id_file']][1][0] = $rgb_background[0];
            $parts_colors[$part['id_file']][1][1] = $rgb_background[1];
            $parts_colors[$part['id_file']][1][2] = $rgb_background[2];
        }
    }
    return $parts_colors;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////// T O O L S  //////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function alert (string$message){
    echo "<script type='text/javascript'> alert('$message');</script>";
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////C O N F I G U R A T I O N ////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function conf_parts_range():array{
    return PARTS_RANGES;
}

function conf_workday():array{
    $workday = WORKDAY;
    
    if (isset($_GET['workday_from']) && isset($_GET['workday_from'])){
        $workday['from_hour']    = explode(':', $_GET['workday_from'])[0];
        $workday['from_minute']  = explode(':', $_GET['workday_from'])[1];
        $workday['to_hour']      = explode(':', $_GET['workday_to'])[0];
        $workday['to_minute']    = explode(':', $_GET['workday_to'])[1];
        //var_dump(explode(':', $_GET['workday_to'])[0]);
        //die;
    }
    
    return $workday;
}

function conf_week_working_days():array{
    return WEEK_WORKING_DAYS;
}

function conf_parts_colors():array{
    return getPartsColors();
}

function conf_delays():array{
    return DELAYS;
}

function conf_working_days():array{
    return WORKING_DAYS;
}

function conf_default_roll_weight():float{
    return DEFAULT_ROLL_WEIGHT;
}

function conf_id_neighborhood_factory():int{
    return ID_NEIGHBORHOOD_FACTORY;
}

function conf_days_out_range_slot():int{
    return DAYS_OUT_RANGE_SLOT;
}
