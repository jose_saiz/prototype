<?php

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// C R E A T E   P A R T S   F R O M   O R D E R //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Part
{
    public $id, $id_file, $id_version, $id_printer, $id_state;
    public $name, $start_datetime, $end_datetime, $weight;
    public $quantity, $left_over;

    function save($id_order)
    {
        //Creo Part
        $sql = sprintf("INSERT INTO part (id_file, id_version, id_order,id_state, weight)
            VALUES (" . $this->id_file . "," . $this->id_version . "," . $id_order . "," . $this->id_state . "," . $this->weight . ");");

        $result = DataBase::getConnection()->query($sql);
        $id_inserted = "";
        if ($result) {
            $id_inserted = (string) mysqli_insert_id(DataBase::getConnection());
        }
        //Actualizo Order Line
        $sql = sprintf("
            SELECT id,ids_parts FROM order_line WHERE id_file = " . $this->id_file . " AND id_version = " . $this->id_version . " AND id_order = " . $id_order . ";");

        $result = DataBase::getConnection()->query($sql);
        $row = $result->fetch_assoc();

        $ids_parts = new ArrayObject(array());
        if ($row['ids_parts'] == "") {
            $ids_parts->append($id_inserted);
        } else {
            foreach (json_decode($row['ids_parts'], false) as $id_part) {
                $ids_parts->append($id_part);
            }
            $ids_parts->append($id_inserted);
        }
        $sql = sprintf("
            UPDATE order_line SET ids_parts = '" . json_encode($ids_parts) . "' WHERE id = " . $row['id'] . ";");

        $result = DataBase::getConnection()->query($sql);
        //Fin Actualizo Order
        print_r('</br>Part Inserted, id:' . $id_inserted);
    }
}

class Parts
{
    public $parts = [
        Part::class
    ];

    function __construct($id_order)
    {
        //Comprobar si el pedido ya tiene creadas Parts
        if (! self::existParts($id_order)) {
            //Crear Parts del pedido
            $sql = sprintf('
                SELECT
                    ol.id,ol.id_order,ol.id_file,ol.id_version,ol.ids_parts,
                    ol.quantity as quantity_x_order,
                    f.quantity as quantity_x_print,
                    f.quantity - ol.quantity as left_over,
                    f.filament_used as weight,
                    CASE
                        WHEN f.quantity - ol.quantity = 0 THEN f.quantity
                        WHEN f.quantity - ol.quantity > 0 THEN 1
                        WHEN f.quantity - ol.quantity < 0 THEN ol.quantity/f.quantity
                    END as quantity,
                    f.estimated_printing_hours,f.estimated_printing_minutes
                FROM order_line AS ol
                INNER JOIN file AS f ON ol.id_file = f.id_file AND ol.id_version = f.id_version
                WHERE id_order  = ' . $id_order . ';');

            //$obj = new stdClass;

            $result = DataBase::getConnection()->query($sql);

            echo "</br>";

            if ($result->num_rows > 0) {
                $parts = [
                    Part::class
                ];
                while ($row = $result->fetch_assoc()) {
                    $quantity = 0;

                    //Calculo de Cantidad
                    if ($row["quantity_x_order"] == $row["quantity_x_print"]) {
                        $quantity = $row["quantity_x_order"];
                    } elseif ($row["quantity_x_order"] < $row["quantity_x_print"]) {
                        $quantity = 1;
                    } elseif ($row["quantity_x_order"] > $row["quantity_x_print"]) {
                        if ($row["quantity_x_order"] % $row["quantity_x_print"] == 0) {
                            $quantity = $row["quantity_x_order"] / $row["quantity_x_print"];
                        } else {
                            $quantity = intdiv($row["quantity_x_order"], $row["quantity_x_print"]) + 1;
                        }
                    }

                    for ($i = 0; $i < $quantity; $i ++) {
                        $part = new Part();
                        $part->id = $row["id"];
                        $part->id_file = $row["id_file"];
                        $part->id_version = $row["id_version"];
                        //$part->id_printer = $row["id_printer"];
                        $part->id_state = 8;
                        $part->weight = $row["weight"];

                        $part->left_over = $row["left_over"];

                        $part->save($id_order);
                        array_push($this->parts, $part);
                    }
                    print_r('</br>');
                }
                echo "<br>Parts: ";
                print_r($this->parts);
                print_r('</br>');
            }
        }
    }

    public function existParts($id_order)
    {
        $sql = sprintf('SELECT * FROM part WHERE id_order = ' . $id_order);
        $result = DataBase::getConnection()->query($sql);
        var_dump(($result->num_rows > 0));
        //die;
        if ($result->num_rows > 0) {
            $message = "Parts already exist for this order, do you want to create reassign them?";
            echo "<script type='text/javascript'> alert('$message');</script>";
            return true;
        } else {
            return false;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//// Crea todas las partes en bdd y actualiza las lineas de pedidos con su parte correspondiente ////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//parts = new Parts(5);
//print_r($parts);
/////////////////////////////////////////////////////////////////////////////////////////////////////
// Fin Crea todas las partes en bdd y actualiza las lineas de pedidos con su parte correspondiente //
/////////////////////////////////////////////////////////////////////////////////////////////////////
?>