<?php

// Db configs.
define('HOST', 'localhost');
define('PORT', 3307);
define('DATABASE', 'greeny-prototype');
define('USERNAME', 'root');
define('PASSWORD', '');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// D A T A   B A S E //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DataBase
{
    private static $db;
    private $connection;

    private function __construct()
    {
        $this->connection = new MySQLi(HOST, USERNAME, PASSWORD, DATABASE, PORT);
    }

    function __destruct()
    {
        $this->connection->close();
    }

    public static function getConnection()
    {
        if (self::$db == null) {
            self::$db = new Database();
        }
        return self::$db->connection;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////// Q U E R I E S //////////////////////////////////////////////////////////////////
    
    
    public static function queryTimeWithoutOperators(int $id_neighborhood_factory){
        $sql = sprintf('
                SELECT *
                FROM  time_without_operators AS two
                WHERE id_neighborhood_factory  = ' . $id_neighborhood_factory . ' AND active = 1
                ORDER BY day;');
        return $sql;
    }
    
    public static function queryAvailablePrinters(int $id_neighborhood_factory){
        $sql = sprintf('
                SELECT  *
                FROM    printer AS pri
                WHERE   available = 1 AND id_neighborhood_factory = ' . $id_neighborhood_factory . '
                ORDER BY pri.id;
               ');
        return $sql;
    }
    
    public static function queryPendingParts(){
        return  sprintf('
                SELECT  p.id,p.id_file,f.name AS file_name, p.id_version,p.id_order,fp.id_product, pr.name AS product_name, p.id_final_product,id_state,p.weight,
                        f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,p.start_datetime,p.initiated
                FROM    part AS p
                INNER   JOIN file AS f ON p.id_file = f.id_file AND p.id_version = f.id_version
                INNER   JOIN final_product AS fp ON fp.id = p.id_final_product
                INNER   JOIN product       AS pr ON pr.id = fp.id_product
                WHERE   id_state = 8
                ORDER BY   p.id_order ASC,p.id_final_product  ASC,  f.estimated_printing_hours DESC, f.estimated_printing_minutes DESC;
                ');
    }
    
    
    public static function chargePrinterTimeline(int $id_printer){
        return sprintf('
            SELECT      pri.id,code,id_neighborhood_factory,pri.id_state AS id_printer_state ,roll_replacement_datetime, roll_weight,
                        f.name AS file_name, pr.name AS product_name, fp.id_product, par.*
            
                FROM    printer AS pri
                LEFT JOIN part AS par ON pri.id = par.id_printer
                INNER JOIN file AS f ON par.id_file = f.id_file AND par.id_version = f.id_version
                INNER   JOIN final_product AS fp ON fp.id = par.id_final_product
                INNER   JOIN product       AS pr ON pr.id = fp.id_product
                WHERE   pri.id = ' . $id_printer. '
                ORDER BY start_datetime, id_order, pri.id;
           ');
        //ORDER BY id_order,start_datetime, pri.id;
    }
    
    
    public static function querySelectReassignPart(int $id_part){
        return sprintf('
                SELECT  p.id,p.id_file, f.name AS file_name, p.id_version,p.id_order,fp.id_product, pr.name AS product_name,id_final_product,id_state,p.weight,
                        f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,
                        p.start_datetime,p.initiated, p.end_datetime,p.id_printer
                FROM    part AS p
                INNER   JOIN file AS f ON p.id_file = f.id_file AND p.id_version = f.id_version
                INNER   JOIN final_product AS fp ON fp.id = p.id_final_product
                INNER   JOIN product       AS pr ON pr.id = fp.id_product
                WHERE   p.id =' . $id_part . ';
                '); 
            //ORDER BY p.id_order  ASC, p.id_final_product  ASC, f.estimated_printing_hours DESC, f.estimated_printing_minutes DESC,p.id;
        
    }
    public static function queryChargePrintersMatrix(int $id_print){
        return sprintf('
                SELECT  pri.id,code,id_neighborhood_factory,pri.id_state AS id_printer_state ,roll_replacement_datetime, roll_weight, 
                        f.name AS file_name, fp.id_product, pr.name AS product_name,id_final_product,par.*
                FROM    printer AS pri
                LEFT JOIN part AS par ON pri.id = par.id_printer
                INNER JOIN file AS f ON par.id_file = f.id_file AND par.id_version = f.id_version
                INNER   JOIN final_product AS fp ON fp.id = par.id_final_product
                INNER   JOIN product       AS pr ON pr.id = fp.id_product
                WHERE   pri.id = ' . $id_print . '
                ORDER BY start_datetime, id_order, pri.id;
           ');
        //ORDER BY id_order,start_datetime, pri.id;
    }
    public static function queryPendingPartsOLD(){
        return  sprintf('
                SELECT  p.id,p.id_file,p.id_version,p.id_order,p.id_final_product,id_state,p.weight,f.name as file_name,f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,p.start_datetime,p.initiated
                FROM    part AS p
                INNER   JOIN file AS f ON p.id_file = f.id_file AND p.id_version = f.id_version
                WHERE   id_state = 8
                ORDER BY   f.estimated_printing_hours DESC, p.id_order,p.id_final_product  ASC,  f.estimated_printing_minutes DESC, p.id;
                ');
    }
    
}
?>