/*
Navicat MySQL Data Transfer

Source Server         : localhostMariaDB
Source Server Version : 50505
Source Host           : localhost:3307
Source Database       : greeny-prototype

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-01-24 22:18:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_contact_type` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of contact
-- ----------------------------
INSERT INTO `contact` VALUES ('1', '1', 'Palma Nova Factory', null, null, null);
INSERT INTO `contact` VALUES ('2', '2', 'Customer 2', null, null, null);
INSERT INTO `contact` VALUES ('3', '2', 'Customer 3', null, null, null);
INSERT INTO `contact` VALUES ('4', '2', 'Customer 4', null, null, null);

-- ----------------------------
-- Table structure for contact_type
-- ----------------------------
DROP TABLE IF EXISTS `contact_type`;
CREATE TABLE `contact_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of contact_type
-- ----------------------------
INSERT INTO `contact_type` VALUES ('1', 'neighborhood_factory', null);
INSERT INTO `contact_type` VALUES ('2', 'customer', null);
INSERT INTO `contact_type` VALUES ('3', 'provider', null);

-- ----------------------------
-- Table structure for filament
-- ----------------------------
DROP TABLE IF EXISTS `filament`;
CREATE TABLE `filament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_provider` int(11) DEFAULT NULL,
  `diameter` decimal(10,2) DEFAULT NULL,
  `length` double(10,2) DEFAULT NULL,
  `reference` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of filament
-- ----------------------------

-- ----------------------------
-- Table structure for filament_characteristics
-- ----------------------------
DROP TABLE IF EXISTS `filament_characteristics`;
CREATE TABLE `filament_characteristics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_filament` int(11) DEFAULT NULL,
  `id_type_filament characteristics` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of filament_characteristics
-- ----------------------------

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id_file` int(11) NOT NULL,
  `id_version` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `estimated_printing_time` varchar(6) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `estimated_printing_hours` int(11) DEFAULT NULL,
  `estimated_printing_minutes` int(11) DEFAULT NULL,
  `estimated_printing_hours_total` int(11) DEFAULT NULL,
  `estimated_printing_minutes_total` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_x_printing` int(11) DEFAULT NULL,
  `quantity_x_tower` int(11) DEFAULT NULL,
  `slicing_progrma` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `parrameters` longblob DEFAULT NULL,
  `filament_used` double(10,2) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `id_type` int(11) DEFAULT NULL,
  `act` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_file`,`id_version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES ('323', '9', '0323v9_N06_BayonetTankWheels_1892g', '76:38', '76', '38', '76', '38', '1', '1', '1', null, null, '1892.00', null, null, null);
INSERT INTO `file` VALUES ('327', '8', '0327v8_N06_PlantCaseBayonet_1013g327', '42:21', '42', '21', '42', '21', '1', '1', '1', null, null, '1013.00', null, null, null);
INSERT INTO `file` VALUES ('294', '27', '0294v27_N06_Tube3x4p56d90_747g', '37:40', '37', '40', '114', '0', '3', '1', '3', null, null, '747.00', null, null, null);
INSERT INTO `file` VALUES ('332', '7', '0332v7_N06_BaseDrainVentilation_1012g', '37:10', '37', '10', '37', '10', '1', '1', '1', null, null, '1012.00', null, null, null);
INSERT INTO `file` VALUES ('322', '5', '0322v5_N06_TubeLampVentilatorHolder_658g', '26:44', '26', '44', '26', '44', '1', '1', '1', null, null, '658.00', null, null, null);
INSERT INTO `file` VALUES ('314', '6', '0314v7_N06_6x_WheelsCap_520g', '21:44', '21', '44', '21', '44', '1', '6', '4', null, null, '520.00', null, null, null);
INSERT INTO `file` VALUES ('283', '8', '0283v8_N06_DT-Draw_575g', '21:25', '21', '25', '21', '25', '1', '1', '1', null, null, '575.00', null, null, null);
INSERT INTO `file` VALUES ('331', '10', '0331v10_N06_PlantCaseGutter_536g', '18:16', '18', '16', '18', '16', '1', '1', '1', null, null, '536.00', null, null, null);
INSERT INTO `file` VALUES ('320', '6', '0320v6_N06_LightProfileHolder_216g', '9:31', '9', '31', '9', '31', '1', '1', '1', null, null, '216.00', null, null, null);
INSERT INTO `file` VALUES ('276', '9', '0276v9_N06_DT-DistrDisc_166g', '7:55', '7', '55', '7', '55', '1', '1', '1', null, null, '166.00', null, null, null);
INSERT INTO `file` VALUES ('303', '12', '0303v12_N06_9xPotholeLidSmall_160g', '5:51', '5', '51', '11', '41', '2', '9', '18', null, null, '160.00', null, null, null);
INSERT INTO `file` VALUES ('338', '4', '0338v4_N06_DrawInset_95g', '3:54', '3', '54', '3', '54', '1', '1', '1', null, null, '95.00', null, null, null);
INSERT INTO `file` VALUES ('341', '3', '0341v3_N06_9x_GutterHoseConnector_80g', '3:13', '3', '13', '3', '13', '1', '9', '2', null, null, '80.00', null, null, null);

-- ----------------------------
-- Table structure for order_g
-- ----------------------------
DROP TABLE IF EXISTS `order_g`;
CREATE TABLE `order_g` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_state` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of order_g
-- ----------------------------
INSERT INTO `order_g` VALUES ('1', '12', '2', '1');
INSERT INTO `order_g` VALUES ('2', '12', '2', '1');
INSERT INTO `order_g` VALUES ('3', '12', '2', '1');
INSERT INTO `order_g` VALUES ('4', '12', '3', '1');
INSERT INTO `order_g` VALUES ('5', '12', '3', '1');
INSERT INTO `order_g` VALUES ('6', '12', '4', '1');
INSERT INTO `order_g` VALUES ('7', '12', '4', '1');
INSERT INTO `order_g` VALUES ('8', '12', '3', '1');

-- ----------------------------
-- Table structure for order_line
-- ----------------------------
DROP TABLE IF EXISTS `order_line`;
CREATE TABLE `order_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_file` int(11) DEFAULT NULL,
  `id_version` int(11) DEFAULT NULL,
  `ids_parts` blob DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_print_x_order_line` int(3) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of order_line
-- ----------------------------
INSERT INTO `order_line` VALUES ('1', '2', null, '323', '9', 0x5B7B226964223A2231227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('2', '2', null, '327', '8', 0x5B7B226964223A2232227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('3', '2', null, '294', '27', 0x5B7B226964223A2233227D2C7B226964223A2234227D2C7B226964223A2235227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('4', '2', null, '332', '7', 0x5B7B226964223A2236227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('5', '2', null, '322', '5', 0x5B7B226964223A2237227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('6', '2', null, '314', '6', 0x5B7B226964223A2238227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('7', '2', null, '283', '8', 0x5B7B226964223A2239227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('8', '2', null, '331', '10', 0x5B7B226964223A223130227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('9', '2', null, '320', '6', 0x5B7B226964223A223131227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('10', '2', null, '276', '9', 0x5B7B226964223A223132227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('11', '2', null, '303', '12', 0x5B7B226964223A223133227D2C7B226964223A223134227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('12', '2', null, '338', '4', 0x5B7B226964223A223135227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('13', '2', null, '341', '3', 0x5B7B226964223A223136227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('14', '3', null, '323', '9', 0x5B7B226964223A223137227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('15', '3', null, '327', '8', 0x5B7B226964223A223138227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('16', '3', null, '294', '27', 0x5B7B226964223A223139227D2C7B226964223A223230227D2C7B226964223A223231227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('17', '3', null, '332', '7', 0x5B7B226964223A223232227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('18', '3', null, '322', '5', 0x5B7B226964223A223233227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('19', '3', null, '314', '6', 0x5B7B226964223A223234227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('20', '3', null, '283', '8', 0x5B7B226964223A223235227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('21', '3', null, '331', '10', 0x5B7B226964223A223236227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('22', '3', null, '320', '6', 0x5B7B226964223A223237227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('23', '3', null, '276', '9', 0x5B7B226964223A223238227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('24', '3', null, '303', '12', 0x5B7B226964223A223239227D2C7B226964223A223330227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('25', '3', null, '338', '4', 0x5B7B226964223A223331227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('26', '3', null, '341', '3', 0x5B7B226964223A223332227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('27', '4', null, '323', '9', 0x5B7B226964223A223333227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('28', '4', null, '327', '8', 0x5B7B226964223A223334227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('29', '4', null, '294', '27', 0x5B7B226964223A223335227D2C7B226964223A223336227D2C7B226964223A223337227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('30', '4', null, '332', '7', 0x5B7B226964223A223338227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('31', '4', null, '322', '5', 0x5B7B226964223A223339227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('32', '4', null, '314', '6', 0x5B7B226964223A223430227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('33', '4', null, '283', '8', 0x5B7B226964223A223431227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('34', '4', null, '331', '10', 0x5B7B226964223A223432227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('35', '4', null, '320', '6', 0x5B7B226964223A223433227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('36', '4', null, '276', '9', 0x5B7B226964223A223434227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('37', '4', null, '303', '12', 0x5B7B226964223A223435227D2C7B226964223A223436227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('38', '4', null, '338', '4', 0x5B7B226964223A223437227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('39', '4', null, '341', '3', 0x5B7B226964223A223438227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('40', '5', null, '323', '9', 0x5B7B226964223A223439227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('41', '5', null, '327', '8', 0x5B7B226964223A223530227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('42', '5', null, '294', '27', 0x5B7B226964223A223531227D2C7B226964223A223532227D2C7B226964223A223533227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('43', '5', null, '332', '7', 0x5B7B226964223A223534227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('44', '5', null, '322', '5', 0x5B7B226964223A223535227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('45', '5', null, '314', '6', 0x5B7B226964223A223536227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('46', '5', null, '283', '8', 0x5B7B226964223A223537227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('47', '5', null, '331', '10', 0x5B7B226964223A223538227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('48', '5', null, '320', '6', 0x5B7B226964223A223539227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('49', '5', null, '276', '9', 0x5B7B226964223A223630227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('50', '5', null, '303', '12', 0x5B7B226964223A223631227D2C7B226964223A223632227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('51', '5', null, '338', '4', 0x5B7B226964223A223633227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('52', '5', null, '341', '3', 0x5B7B226964223A223634227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('53', '6', null, '323', '9', 0x5B7B226964223A223635227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('54', '6', null, '327', '8', 0x5B7B226964223A223636227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('55', '6', null, '294', '27', 0x5B7B226964223A223637227D2C7B226964223A223638227D2C7B226964223A223639227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('56', '6', null, '332', '7', 0x5B7B226964223A223730227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('57', '6', null, '322', '5', 0x5B7B226964223A223731227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('58', '6', null, '314', '6', 0x5B7B226964223A223732227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('59', '6', null, '283', '8', 0x5B7B226964223A223733227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('60', '6', null, '331', '10', 0x5B7B226964223A223734227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('61', '6', null, '320', '6', 0x5B7B226964223A223735227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('62', '6', null, '276', '9', 0x5B7B226964223A223736227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('63', '6', null, '303', '12', 0x5B7B226964223A223737227D2C7B226964223A223738227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('64', '6', null, '338', '4', 0x5B7B226964223A223739227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('65', '6', null, '341', '3', 0x5B7B226964223A223830227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('66', '7', null, '323', '9', 0x5B7B226964223A223831227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('67', '7', null, '327', '8', 0x5B7B226964223A223832227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('68', '7', null, '294', '27', 0x5B7B226964223A223833227D2C7B226964223A223834227D2C7B226964223A223835227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('69', '7', null, '332', '7', 0x5B7B226964223A223836227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('70', '7', null, '322', '5', 0x5B7B226964223A223837227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('71', '7', null, '314', '6', 0x5B7B226964223A223838227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('72', '7', null, '283', '8', 0x5B7B226964223A223839227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('73', '7', null, '331', '10', 0x5B7B226964223A223930227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('74', '7', null, '320', '6', 0x5B7B226964223A223931227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('75', '7', null, '276', '9', 0x5B7B226964223A223932227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('76', '7', null, '303', '12', 0x5B7B226964223A223933227D2C7B226964223A223934227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('77', '7', null, '338', '4', 0x5B7B226964223A223935227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('78', '7', null, '341', '3', 0x5B7B226964223A223936227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('79', '8', null, '323', '9', 0x5B7B226964223A223937227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('80', '8', null, '327', '8', 0x5B7B226964223A223938227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('81', '8', null, '294', '27', 0x5B7B226964223A223939227D2C7B226964223A22313030227D2C7B226964223A22313031227D5D, '3', '3', null);
INSERT INTO `order_line` VALUES ('82', '8', null, '332', '7', 0x5B7B226964223A22313032227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('83', '8', null, '322', '5', 0x5B7B226964223A22313033227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('84', '8', null, '314', '6', 0x5B7B226964223A22313034227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('85', '8', null, '283', '8', 0x5B7B226964223A22313035227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('86', '8', null, '331', '10', 0x5B7B226964223A22313036227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('87', '8', null, '320', '6', 0x5B7B226964223A22313037227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('88', '8', null, '276', '9', 0x5B7B226964223A22313038227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('89', '8', null, '303', '12', 0x5B7B226964223A22313039227D2C7B226964223A22313130227D5D, '2', '2', null);
INSERT INTO `order_line` VALUES ('90', '8', null, '338', '4', 0x5B7B226964223A22313131227D5D, '1', '1', null);
INSERT INTO `order_line` VALUES ('91', '8', null, '341', '3', 0x5B7B226964223A22313132227D5D, '1', '1', null);

-- ----------------------------
-- Table structure for part
-- ----------------------------
DROP TABLE IF EXISTS `part`;
CREATE TABLE `part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_file` int(11) DEFAULT NULL,
  `id_version` int(11) DEFAULT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_filament` int(11) DEFAULT NULL,
  `id_printer` int(11) DEFAULT NULL,
  `id_state` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT '',
  `start_datetime` datetime DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `weight` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of part
-- ----------------------------
INSERT INTO `part` VALUES ('1', '323', '9', '2', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('2', '327', '8', '2', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('3', '294', '27', '2', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('4', '294', '27', '2', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('5', '294', '27', '2', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('6', '332', '7', '2', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('7', '322', '5', '2', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('8', '314', '6', '2', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('9', '283', '8', '2', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('10', '331', '10', '2', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('11', '320', '6', '2', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('12', '276', '9', '2', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('13', '303', '12', '2', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('14', '303', '12', '2', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('15', '338', '4', '2', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('16', '341', '3', '2', null, null, '8', '', null, null, '80.00');
INSERT INTO `part` VALUES ('17', '323', '9', '3', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('18', '327', '8', '3', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('19', '294', '27', '3', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('20', '294', '27', '3', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('21', '294', '27', '3', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('22', '332', '7', '3', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('23', '322', '5', '3', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('24', '314', '6', '3', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('25', '283', '8', '3', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('26', '331', '10', '3', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('27', '320', '6', '3', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('28', '276', '9', '3', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('29', '303', '12', '3', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('30', '303', '12', '3', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('31', '338', '4', '3', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('32', '341', '3', '3', null, null, '8', '', null, null, '80.00');
INSERT INTO `part` VALUES ('33', '323', '9', '4', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('34', '327', '8', '4', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('35', '294', '27', '4', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('36', '294', '27', '4', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('37', '294', '27', '4', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('38', '332', '7', '4', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('39', '322', '5', '4', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('40', '314', '6', '4', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('41', '283', '8', '4', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('42', '331', '10', '4', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('43', '320', '6', '4', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('44', '276', '9', '4', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('45', '303', '12', '4', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('46', '303', '12', '4', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('47', '338', '4', '4', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('48', '341', '3', '4', null, null, '8', '', null, null, '80.00');
INSERT INTO `part` VALUES ('49', '323', '9', '5', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('50', '327', '8', '5', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('51', '294', '27', '5', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('52', '294', '27', '5', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('53', '294', '27', '5', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('54', '332', '7', '5', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('55', '322', '5', '5', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('56', '314', '6', '5', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('57', '283', '8', '5', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('58', '331', '10', '5', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('59', '320', '6', '5', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('60', '276', '9', '5', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('61', '303', '12', '5', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('62', '303', '12', '5', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('63', '338', '4', '5', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('64', '341', '3', '5', null, null, '8', '', null, null, '80.00');
INSERT INTO `part` VALUES ('65', '323', '9', '6', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('66', '327', '8', '6', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('67', '294', '27', '6', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('68', '294', '27', '6', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('69', '294', '27', '6', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('70', '332', '7', '6', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('71', '322', '5', '6', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('72', '314', '6', '6', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('73', '283', '8', '6', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('74', '331', '10', '6', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('75', '320', '6', '6', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('76', '276', '9', '6', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('77', '303', '12', '6', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('78', '303', '12', '6', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('79', '338', '4', '6', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('80', '341', '3', '6', null, null, '8', '', null, null, '80.00');
INSERT INTO `part` VALUES ('81', '323', '9', '7', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('82', '327', '8', '7', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('83', '294', '27', '7', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('84', '294', '27', '7', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('85', '294', '27', '7', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('86', '332', '7', '7', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('87', '322', '5', '7', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('88', '314', '6', '7', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('89', '283', '8', '7', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('90', '331', '10', '7', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('91', '320', '6', '7', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('92', '276', '9', '7', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('93', '303', '12', '7', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('94', '303', '12', '7', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('95', '338', '4', '7', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('96', '341', '3', '7', null, null, '8', '', null, null, '80.00');
INSERT INTO `part` VALUES ('97', '323', '9', '8', null, null, '8', '', null, null, '1892.00');
INSERT INTO `part` VALUES ('98', '327', '8', '8', null, null, '8', '', null, null, '1013.00');
INSERT INTO `part` VALUES ('99', '294', '27', '8', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('100', '294', '27', '8', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('101', '294', '27', '8', null, null, '8', '', null, null, '747.00');
INSERT INTO `part` VALUES ('102', '332', '7', '8', null, null, '8', '', null, null, '1012.00');
INSERT INTO `part` VALUES ('103', '322', '5', '8', null, null, '8', '', null, null, '658.00');
INSERT INTO `part` VALUES ('104', '314', '6', '8', null, null, '8', '', null, null, '520.00');
INSERT INTO `part` VALUES ('105', '283', '8', '8', null, null, '8', '', null, null, '575.00');
INSERT INTO `part` VALUES ('106', '331', '10', '8', null, null, '8', '', null, null, '536.00');
INSERT INTO `part` VALUES ('107', '320', '6', '8', null, null, '8', '', null, null, '216.00');
INSERT INTO `part` VALUES ('108', '276', '9', '8', null, null, '8', '', null, null, '166.00');
INSERT INTO `part` VALUES ('109', '303', '12', '8', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('110', '303', '12', '8', null, null, '8', '', null, null, '160.00');
INSERT INTO `part` VALUES ('111', '338', '4', '8', null, null, '8', '', null, null, '95.00');
INSERT INTO `part` VALUES ('112', '341', '3', '8', null, null, '8', '', null, null, '80.00');

-- ----------------------------
-- Table structure for printer
-- ----------------------------
DROP TABLE IF EXISTS `printer`;
CREATE TABLE `printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `id_neighborhood_factory` int(11) DEFAULT NULL,
  `id_filament` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT '',
  `estimated_availability_datetime` datetime DEFAULT NULL,
  `estimated_filament_finish_datetime` datetime DEFAULT NULL,
  `current_filament_weight` decimal(10,2) DEFAULT NULL,
  `nozzle_diameter` decimal(10,2) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `id_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of printer
-- ----------------------------
INSERT INTO `printer` VALUES ('1', 'T-01', '1', null, null, '', null, null, null, '0.60', '1', null);
INSERT INTO `printer` VALUES ('2', 'T-02', '1', null, null, '', null, null, null, '0.60', '1', null);
INSERT INTO `printer` VALUES ('3', 'T-03', '1', null, null, '', null, null, null, '0.60', '1', null);
INSERT INTO `printer` VALUES ('4', 'T-04', '1', null, null, '', null, null, null, '0.60', '1', null);
INSERT INTO `printer` VALUES ('5', 'T-05', '1', null, null, '', null, null, null, '0.60', '1', null);
INSERT INTO `printer` VALUES ('6', 'T-06', '1', null, null, '', null, null, null, '0.60', '1', null);
INSERT INTO `printer` VALUES ('7', 'T-07', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('8', 'T-08', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('9', 'T-09', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('10', 'T-10', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('11', 'T-11', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('12', 'T-12', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('13', 'T-13', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('14', 'T-14', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('15', 'T-15', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('16', 'T-16', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('17', 'T-17', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('18', 'T-18', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('19', 'T-19', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('20', 'T-20', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('21', 'T-21', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('22', 'T-22', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('23', 'T-23', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('24', 'T-24', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('29', 'T-29', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('56', 'G-56', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('57', 'G-57', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('58', 'G-58', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('59', 'G-59', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('60', 'G-60', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('61', 'G-61', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('62', 'G-62', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('63', 'G-63', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('64', 'G-64', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('65', 'G-65', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('66', 'G-66', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('67', 'G-67', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('68', 'G-68', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('69', 'G-69', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('70', 'G-70', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('71', 'G-71', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('72', 'G-72', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('73', 'G-73', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('74', 'G-74', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('75', 'G-75', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('76', 'G-76', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('77', 'G-77', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('78', 'G-78', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('79', 'G-79', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('80', 'G-80', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('81', 'G-81', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('82', 'G-82', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('117', 'G-117', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('137', 'G-137', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('138', 'G-138', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('139', 'G-139', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('140', 'G-140', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('141', 'G-141', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('142', 'G-142', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('143', 'G-143', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('144', 'G-144', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('145', 'G-145', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('146', 'G-146', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('147', 'G-147', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('148', 'G-148', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('149', 'G-149', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('150', 'G-150', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('151', 'G-151', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('152', 'G-152', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('153', 'G-153', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('154', 'G-154', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('155', 'G-155', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('156', 'G-156', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('157', 'G-157', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('158', 'G-158', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('159', 'G-159', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('160', 'G-160', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('161', 'G-161', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('162', 'G-162', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('163', 'G-163', '1', null, null, '', null, null, null, '0.60', null, null);
INSERT INTO `printer` VALUES ('178', 'G-178', '1', null, null, '', null, null, null, '0.60', null, null);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'greenyGarden Indoor', 'with light and fans');
INSERT INTO `product` VALUES ('2', 'greenyGarden Outdoor ', 'without light and fans');

-- ----------------------------
-- Table structure for product_quantity_parts
-- ----------------------------
DROP TABLE IF EXISTS `product_quantity_parts`;
CREATE TABLE `product_quantity_parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `id_file` int(11) DEFAULT NULL,
  `id_version` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of product_quantity_parts
-- ----------------------------
INSERT INTO `product_quantity_parts` VALUES ('1', '1', '323', '9', '1');
INSERT INTO `product_quantity_parts` VALUES ('2', '1', '327', '8', '1');
INSERT INTO `product_quantity_parts` VALUES ('3', '1', '294', '27', '3');
INSERT INTO `product_quantity_parts` VALUES ('4', '1', '332', '7', '1');
INSERT INTO `product_quantity_parts` VALUES ('5', '1', '322', '5', '1');
INSERT INTO `product_quantity_parts` VALUES ('6', '1', '314', '6', '1');
INSERT INTO `product_quantity_parts` VALUES ('7', '1', '283', '8', '1');
INSERT INTO `product_quantity_parts` VALUES ('8', '1', '331', '10', '1');
INSERT INTO `product_quantity_parts` VALUES ('9', '1', '320', '6', '1');
INSERT INTO `product_quantity_parts` VALUES ('10', '1', '276', '9', '1');
INSERT INTO `product_quantity_parts` VALUES ('11', '1', '303', '12', '2');
INSERT INTO `product_quantity_parts` VALUES ('12', '1', '338', '4', '1');
INSERT INTO `product_quantity_parts` VALUES ('13', '1', '341', '3', '1');
INSERT INTO `product_quantity_parts` VALUES ('14', '2', '323', '9', '1');
INSERT INTO `product_quantity_parts` VALUES ('15', '2', '327', '8', '1');
INSERT INTO `product_quantity_parts` VALUES ('16', '2', '294', '27', '3');
INSERT INTO `product_quantity_parts` VALUES ('17', '2', '332', '7', '1');
INSERT INTO `product_quantity_parts` VALUES ('18', '2', '322', '5', '1');
INSERT INTO `product_quantity_parts` VALUES ('19', '2', '314', '6', '1');
INSERT INTO `product_quantity_parts` VALUES ('20', '2', '283', '8', '1');
INSERT INTO `product_quantity_parts` VALUES ('21', '2', '331', '10', '1');
INSERT INTO `product_quantity_parts` VALUES ('22', '2', '320', '6', '1');
INSERT INTO `product_quantity_parts` VALUES ('23', '2', '276', '9', '1');
INSERT INTO `product_quantity_parts` VALUES ('24', '2', '303', '12', '2');
INSERT INTO `product_quantity_parts` VALUES ('25', '2', '338', '4', '1');
INSERT INTO `product_quantity_parts` VALUES ('26', '2', '341', '3', '1');

-- ----------------------------
-- Table structure for state
-- ----------------------------
DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_state_type` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of state
-- ----------------------------
INSERT INTO `state` VALUES ('1', '1', 'Working Printer', null);
INSERT INTO `state` VALUES ('2', '1', 'Stop Printer', null);
INSERT INTO `state` VALUES ('3', '1', 'Broken Printer', null);
INSERT INTO `state` VALUES ('4', '1', 'No Filament  Printer', null);
INSERT INTO `state` VALUES ('5', '1', 'Begin  Printer', null);
INSERT INTO `state` VALUES ('6', '1', 'End  Printer', null);
INSERT INTO `state` VALUES ('7', '3', 'Test Part', null);
INSERT INTO `state` VALUES ('8', '3', 'Created Part', null);
INSERT INTO `state` VALUES ('9', '3', 'Assigned Part', null);
INSERT INTO `state` VALUES ('10', '3', 'OK Part', null);
INSERT INTO `state` VALUES ('11', '3', 'Stock Part', null);
INSERT INTO `state` VALUES ('12', '4', ' Create Order', null);

-- ----------------------------
-- Table structure for state_type
-- ----------------------------
DROP TABLE IF EXISTS `state_type`;
CREATE TABLE `state_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of state_type
-- ----------------------------
INSERT INTO `state_type` VALUES ('1', 'Printer', null);
INSERT INTO `state_type` VALUES ('2', 'File', null);
INSERT INTO `state_type` VALUES ('3', 'Part', null);
INSERT INTO `state_type` VALUES ('4', 'Order', null);

-- ----------------------------
-- Table structure for stock
-- ----------------------------
DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_file` int(11) DEFAULT NULL,
  `id_version` int(11) DEFAULT NULL,
  `quantity` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of stock
-- ----------------------------

-- ----------------------------
-- Table structure for type_filament_characteristics
-- ----------------------------
DROP TABLE IF EXISTS `type_filament_characteristics`;
CREATE TABLE `type_filament_characteristics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of type_filament_characteristics
-- ----------------------------
INSERT INTO `type_filament_characteristics` VALUES ('1', 'color', null);
INSERT INTO `type_filament_characteristics` VALUES ('2', 'material', null);

-- ----------------------------
-- Table structure for workday
-- ----------------------------
DROP TABLE IF EXISTS `workday`;
CREATE TABLE `workday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_neighborhood_factory` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `begin_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `bane` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of workday
-- ----------------------------
