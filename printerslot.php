<?php

///////////////////////////////////////////////////////////// P R I N T E R S L O T  - C L A S S ////////////////////////////////////////////////////////////////
class PrinterSlot
{
    public $part;
    public $order;
    public $file;
    public $file_name;
    public $version;
    public $product;
    public $product_name;
    public $final_product;
    public $start;
    public $end;
    public $weight;
    public $state;
    public $initiated;
    public $minutes;

    function __construct($part, $order, $file, $file_name, $version, $product, $product_name, $final_product, $start, $end, $weight, $state,$initiated)
    {

        
        $this->part = $part;
        $this->order = $order;
        $this->file = $file;
        $this->file_name = $file_name;
        $this->version = $version;
        $this->product = $product;
        $this->product_name = $product_name;
        $this->final_product = $final_product;
        $this->start = $start;
        $this->end = $end;
        $this->weight = $weight;
        $this->state = $state;
        $this->initiated = $initiated;
        $this->minutes = $this->minutes();
        
    }
    
    
    
    static function  new(PrinterSlot $printerSlot)
    {
        $new = new PrinterSlot( $printerSlot->part, 
                                $printerSlot->order, 
                                $printerSlot->file,
                                $printerSlot->file_name,
                                $printerSlot->version,
                                $printerSlot->product,
                                $printerSlot->product_name,
                                $printerSlot->final_product, 
                                $printerSlot->start, 
                                $printerSlot->end, 
                                $printerSlot->weight, 
                                $printerSlot->state, 
                                $printerSlot->initiated,
                                $printerSlot->minutes);
        
        return $new;
        
    }

    function update(PrinterSlot $printerSlot)
    {
        $this->part = $printerSlot->part;
        $this->order = $printerSlot->order;
        $this->file = $printerSlot->file;
        $this->file_name = $printerSlot->file_name;
        $this->version = $printerSlot->version;
        $this->product =  $printerSlot->product;
        $this->product_name =  $printerSlot->product_name;
        $this->final_product =  $printerSlot->final_product;
        $this->start = $printerSlot->start;
        $this->end = $printerSlot->end;
        $this->weight = $printerSlot->weight;
        $this->state = $printerSlot->state;
        $this->initiated = $printerSlot->initiated;
        $this->minutes = $printerSlot->minutes;
        
    }
    
    function toPendingPart():array
    {
        
        $diff =  DateTimeG::newDateTime($this->start)->diffRangeHM(DateTimeG::newDateTime($this->end));
        $pending_part = array();
        $pending_part ['estimated_printing_hours'] = $diff['hours'];
        $pending_part ['estimated_printing_minutes'] = $diff['minutes'];
        
        $pending_part ['id'] = $this->part;
        $pending_part ['id_order'] = $this->order;
        $pending_part ['id_file'] = $this->file;
        $pending_part ['file_name'] = $this->file_name;
        $pending_part ['id_version'] = $this->version;
        $pending_part ['id_product'] = $this->product;
        $pending_part ['product_name'] = $this->product_name;
        $pending_part ['id_final_product'] = $this->final_product;
        $pending_part ['weight'] = $this->weight;
        $pending_part ['id_state'] = $this->state;
        $pending_part ['initiated'] = $this->initiated;
        $pending_part ['minutes'] = $this->initiated;
        
        return $pending_part;
    }
    static function fromPendingPart(array $pending_part,$start)
    {
        $end = new DateTimeG($start);
        $end ->modify('+'.$pending_part ['estimated_printing_hours'].' hours');
        $end ->modify('+'.$pending_part ['estimated_printing_minutes'].' minutes');
        $printerSlot = new PrinterSlot(
                                $pending_part ['id'], 
                                $pending_part ['id_order'], 
                                $pending_part ['id_file'] , 
                                $pending_part ['file_name'] ,
                                $pending_part ['id_version'], 
                                $pending_part ['id_product'], 
                                $pending_part ['product_name'], 
                                $pending_part ['id_final_product'], 
                                $start, 
                                $end->format('Y-m-d H:i:s'), 
                                $pending_part ['weight'], 
                                $pending_part ['id_state'], 
                                $pending_part ['initiated'],
                                $pending_part ['minutes']);
        return $printerSlot;
    }
    
    private function minutes(){
        $slot_start = DateTimeG::newDateTime($this->start);
        $slot_end = DateTimeG::newDateTime($this->end);
        return $slot_start->diffMinutes($slot_end);
    }
    
}
